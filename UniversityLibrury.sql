ALTER DATABASE [UniversityLibrury] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [UniversityLibrury].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [UniversityLibrury] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [UniversityLibrury] SET ANSI_NULLS OFF
GO
ALTER DATABASE [UniversityLibrury] SET ANSI_PADDING OFF
GO
ALTER DATABASE [UniversityLibrury] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [UniversityLibrury] SET ARITHABORT OFF
GO
ALTER DATABASE [UniversityLibrury] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [UniversityLibrury] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [UniversityLibrury] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [UniversityLibrury] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [UniversityLibrury] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [UniversityLibrury] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [UniversityLibrury] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [UniversityLibrury] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [UniversityLibrury] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [UniversityLibrury] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [UniversityLibrury] SET  DISABLE_BROKER
GO
ALTER DATABASE [UniversityLibrury] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [UniversityLibrury] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [UniversityLibrury] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [UniversityLibrury] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [UniversityLibrury] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [UniversityLibrury] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [UniversityLibrury] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [UniversityLibrury] SET  READ_WRITE
GO
ALTER DATABASE [UniversityLibrury] SET RECOVERY FULL
GO
ALTER DATABASE [UniversityLibrury] SET  MULTI_USER
GO
ALTER DATABASE [UniversityLibrury] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [UniversityLibrury] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'UniversityLibrury', N'ON'
GO
USE [UniversityLibrury]
GO
/****** Object:  Table [dbo].[Authors]    Script Date: 01/27/2016 14:06:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Authors](
	[ID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Authors] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Authors] ([ID], [Name]) VALUES (N'a648359a-b432-438f-99a4-387f45f5d725', N'А. Гудок')
INSERT [dbo].[Authors] ([ID], [Name]) VALUES (N'334834cc-8be4-4b93-a576-41efd0570b82', N'Просто Автор')
INSERT [dbo].[Authors] ([ID], [Name]) VALUES (N'd2d3e778-3c8d-4ff0-8e3b-433d79fb9b4a', N'О. Генри')
INSERT [dbo].[Authors] ([ID], [Name]) VALUES (N'81f212d2-481a-4257-88e1-523f91c7a74f', N'И. Лукашин')
INSERT [dbo].[Authors] ([ID], [Name]) VALUES (N'3e01fd85-ce9c-4d0a-94a2-5fa0ac6f3293', N'Братья Вайнеры')
INSERT [dbo].[Authors] ([ID], [Name]) VALUES (N'fded7da6-9a8d-4892-ace2-7c427faf7d7b', N'Марьянов')
INSERT [dbo].[Authors] ([ID], [Name]) VALUES (N'4a3efdd8-2125-4fef-a5a1-9a091e6b1801', N'Тестовый Автор')
INSERT [dbo].[Authors] ([ID], [Name]) VALUES (N'aa533fd7-061e-4494-8015-9b45779cdaf5', N'Эрих Мария')
INSERT [dbo].[Authors] ([ID], [Name]) VALUES (N'61d64646-8f95-45a4-b6cc-a8f91411201b', N'Достоевский')
INSERT [dbo].[Authors] ([ID], [Name]) VALUES (N'9bdf4087-9722-4e62-99f9-cbc995765979', N'Рихтер')
INSERT [dbo].[Authors] ([ID], [Name]) VALUES (N'1d81d8cc-b383-4344-a758-d31b59ac5462', N'Агния Барто')
INSERT [dbo].[Authors] ([ID], [Name]) VALUES (N'afaf5a47-9ea1-4537-81e4-f2926339148d', N'1')
/****** Object:  Table [dbo].[Users]    Script Date: 01/27/2016 14:06:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[ID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [uniqueidentifier] NOT NULL,
	[IsAdmin] [bit] NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Users] ([ID], [Name], [UserName], [Password], [IsAdmin]) VALUES (N'1b881899-426f-470a-8cef-26e3cc48860b', N'Yan', N'Yan', N'db882899-416f-470a-8cef-26eecc488602', 0)
INSERT [dbo].[Users] ([ID], [Name], [UserName], [Password], [IsAdmin]) VALUES (N'2b881899-426f-470a-8cef-26e3cc48860b', N'Sasha', N'Sasha', N'3b881899-426f-470a-8cef-26e3cc48860b', 0)
INSERT [dbo].[Users] ([ID], [Name], [UserName], [Password], [IsAdmin]) VALUES (N'00f15812-5c31-4788-a34c-adae66e9e1c9', N'2', N'2', N'06d49632-c9dc-9bcb-62ae-aef99612ba6b', 0)
INSERT [dbo].[Users] ([ID], [Name], [UserName], [Password], [IsAdmin]) VALUES (N'080cf831-d008-4286-b042-f9473c70e807', N'1', N'1', N'06d49632-c9dc-9bcb-62ae-aef99612ba6b', 1)
/****** Object:  Table [dbo].[Items]    Script Date: 01/27/2016 14:06:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Items](
	[ID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Publicher] [nvarchar](50) NOT NULL,
	[PublichedDate] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Items] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Items] ([ID], [Name], [Publicher], [PublichedDate]) VALUES (N'db881899-426f-470a-8cef-26eecc48860b', N'Три наперстка', N'Харьков', N'May  7 1982 12:00AM')
INSERT [dbo].[Items] ([ID], [Name], [Publicher], [PublichedDate]) VALUES (N'a30c5be1-3636-42b0-8bac-460c591e4d5c', N'Тестовая статья', N'Тестовый издатель', N'Sep  9 1999 12:00AM')
INSERT [dbo].[Items] ([ID], [Name], [Publicher], [PublichedDate]) VALUES (N'0f8f4d5b-d9cb-469f-a165-70674728850e', N'Введение в ADO.NET', N'Винница', N'Nov 12 2010 12:00AM')
INSERT [dbo].[Items] ([ID], [Name], [Publicher], [PublichedDate]) VALUES (N'0f8f4d5b-d9cb-469f-a165-70865728950e', N'Три товарища', N'Петербург', N'Aug  4 1990 12:00AM')
INSERT [dbo].[Items] ([ID], [Name], [Publicher], [PublichedDate]) VALUES (N'0f2f4d5b-d9cb-469f-a165-70867728950e', N'Бурда мода', N'Питер', N'Jan 12 1985 12:00AM')
INSERT [dbo].[Items] ([ID], [Name], [Publicher], [PublichedDate]) VALUES (N'0f8fad5b-d9cb-469f-a165-70867728950e', N'Техника молодежи', N'Харьков', N'Jun  5 1987 12:00AM')
INSERT [dbo].[Items] ([ID], [Name], [Publicher], [PublichedDate]) VALUES (N'b3bb4c82-aa63-4904-9077-71c5fa2eca20', N'1', N'1', N'1')
INSERT [dbo].[Items] ([ID], [Name], [Publicher], [PublichedDate]) VALUES (N'0f8f4d5b-d9cb-469f-a165-74267728950e', N'Наука и жизнь', N'Москва', N'Dec 12 1992 12:00AM')
INSERT [dbo].[Items] ([ID], [Name], [Publicher], [PublichedDate]) VALUES (N'028f4d5b-d9cb-469f-a165-74867728950e', N'Космос', N'Лениздат', N'Nov  5 2000 12:00AM')
INSERT [dbo].[Items] ([ID], [Name], [Publicher], [PublichedDate]) VALUES (N'0f8f4d5b-d9cb-469f-a165-74867728950e', N'Эра милосердия', N'Лениздат', N'Apr  7 1937 12:00AM')
INSERT [dbo].[Items] ([ID], [Name], [Publicher], [PublichedDate]) VALUES (N'f27e810e-a9bb-42a0-8f82-7c70c38b9b9c', N'Преступление и наказание', N'Москва', N'Dec 12 1917 12:00AM')
INSERT [dbo].[Items] ([ID], [Name], [Publicher], [PublichedDate]) VALUES (N'5a0639c1-6183-47c9-b8a1-a75b8e029684', N'Дары Волхов', N'Харьков', N'Mar 14 1957 12:00AM')
INSERT [dbo].[Items] ([ID], [Name], [Publicher], [PublichedDate]) VALUES (N'5a0639c1-6183-47c9-b8a1-a75b8e029685', N'Введение в ADO.NET', N'Винница', N'Dec 12 2010 12:00AM')
INSERT [dbo].[Items] ([ID], [Name], [Publicher], [PublichedDate]) VALUES (N'c12daa3b-b7bf-4f7f-9fbe-b8d8a67b295e', N'Огнево', N'Харьков', N'May  5 1999 12:00AM')
INSERT [dbo].[Items] ([ID], [Name], [Publicher], [PublichedDate]) VALUES (N'f343a730-7cd6-4ea8-9fdb-b922cab253d2', N'Тестовая книга', N'Тестовый издатель', N'Sep  9 1999 12:00AM')
INSERT [dbo].[Items] ([ID], [Name], [Publicher], [PublichedDate]) VALUES (N'7c9e6679-7425-40de-944b-e07fc1f90ae2', N'Бурда мода', N'Питер', N'Dec 12 1984 12:00AM')
INSERT [dbo].[Items] ([ID], [Name], [Publicher], [PublichedDate]) VALUES (N'7c9e6689-7425-40de-944b-e07fc1f90ae7', N'c3 via CLR', N'Вильямс', N'Aug  8 1988 12:00AM')
INSERT [dbo].[Items] ([ID], [Name], [Publicher], [PublichedDate]) VALUES (N'002d05e0-1419-44d4-9795-e40051ed75c6', N'Последний лист', N'Харьков2', N'Jul  5 1988 12:00AM')
INSERT [dbo].[Items] ([ID], [Name], [Publicher], [PublichedDate]) VALUES (N'552d6ee1-2865-4818-8522-f96f4afb8481', N'Техника молодежи', N'Харьков', N'May  5 1987 12:00AM')
/****** Object:  Table [dbo].[Books]    Script Date: 01/27/2016 14:06:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Books](
	[ItemID] [uniqueidentifier] NOT NULL,
	[AuthorID] [uniqueidentifier] NOT NULL,
	[ISBN] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Books] PRIMARY KEY CLUSTERED 
(
	[ItemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Books] ([ItemID], [AuthorID], [ISBN]) VALUES (N'0f8f4d5b-d9cb-469f-a165-70865728950e', N'aa533fd7-061e-4494-8015-9b45779cdaf5', N'5')
INSERT [dbo].[Books] ([ItemID], [AuthorID], [ISBN]) VALUES (N'f27e810e-a9bb-42a0-8f82-7c70c38b9b9c', N'61d64646-8f95-45a4-b6cc-a8f91411201b', N'123')
INSERT [dbo].[Books] ([ItemID], [AuthorID], [ISBN]) VALUES (N'5a0639c1-6183-47c9-b8a1-a75b8e029684', N'd2d3e778-3c8d-4ff0-8e3b-433d79fb9b4a', N'2236122566')
INSERT [dbo].[Books] ([ItemID], [AuthorID], [ISBN]) VALUES (N'f343a730-7cd6-4ea8-9fdb-b922cab253d2', N'4a3efdd8-2125-4fef-a5a1-9a091e6b1801', N'333')
INSERT [dbo].[Books] ([ItemID], [AuthorID], [ISBN]) VALUES (N'7c9e6689-7425-40de-944b-e07fc1f90ae7', N'9bdf4087-9722-4e62-99f9-cbc995765979', N'2234324523')
INSERT [dbo].[Books] ([ItemID], [AuthorID], [ISBN]) VALUES (N'002d05e0-1419-44d4-9795-e40051ed75c6', N'd2d3e778-3c8d-4ff0-8e3b-433d79fb9b4a', N'2266111566')
/****** Object:  Table [dbo].[Articles]    Script Date: 01/27/2016 14:06:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Articles](
	[ItemID] [uniqueidentifier] NOT NULL,
	[AuthorID] [uniqueidentifier] NOT NULL,
	[Version] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Articles] PRIMARY KEY CLUSTERED 
(
	[ItemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Articles] ([ItemID], [AuthorID], [Version]) VALUES (N'a30c5be1-3636-42b0-8bac-460c591e4d5c', N'fded7da6-9a8d-4892-ace2-7c427faf7d7b', N'1')
INSERT [dbo].[Articles] ([ItemID], [AuthorID], [Version]) VALUES (N'b3bb4c82-aa63-4904-9077-71c5fa2eca20', N'afaf5a47-9ea1-4537-81e4-f2926339148d', N'1')
/****** Object:  Table [dbo].[Copies]    Script Date: 01/27/2016 14:06:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Copies](
	[ID] [uniqueidentifier] NOT NULL,
	[ItemID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Copies] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Copies] ([ID], [ItemID]) VALUES (N'333e6689-7425-40de-944b-e07fc1f90ae7', N'7c9e6689-7425-40de-944b-e07fc1f90ae7')
INSERT [dbo].[Copies] ([ID], [ItemID]) VALUES (N'555e6689-7425-40de-944b-e07fc1f90ae7', N'7c9e6689-7425-40de-944b-e07fc1f90ae7')
/****** Object:  Table [dbo].[Magazines]    Script Date: 01/27/2016 14:06:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Magazines](
	[ItemID] [uniqueidentifier] NOT NULL,
	[IssueNumber] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Magazines] PRIMARY KEY CLUSTERED 
(
	[ItemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Magazines] ([ItemID], [IssueNumber]) VALUES (N'0f2f4d5b-d9cb-469f-a165-70867728950e', N'1')
INSERT [dbo].[Magazines] ([ItemID], [IssueNumber]) VALUES (N'0f8fad5b-d9cb-469f-a165-70867728950e', N'2')
INSERT [dbo].[Magazines] ([ItemID], [IssueNumber]) VALUES (N'0f8f4d5b-d9cb-469f-a165-74267728950e', N'3')
INSERT [dbo].[Magazines] ([ItemID], [IssueNumber]) VALUES (N'7c9e6679-7425-40de-944b-e07fc1f90ae2', N'4')
INSERT [dbo].[Magazines] ([ItemID], [IssueNumber]) VALUES (N'552d6ee1-2865-4818-8522-f96f4afb8481', N'5')
/****** Object:  Table [dbo].[ArticlesInMagazines]    Script Date: 01/27/2016 14:06:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ArticlesInMagazines](
	[ArticleID] [uniqueidentifier] NOT NULL,
	[MagazineID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
INSERT [dbo].[ArticlesInMagazines] ([ArticleID], [MagazineID]) VALUES (N'a30c5be1-3636-42b0-8bac-460c591e4d5c', N'0f8f4d5b-d9cb-469f-a165-74267728950e')
INSERT [dbo].[ArticlesInMagazines] ([ArticleID], [MagazineID]) VALUES (N'b3bb4c82-aa63-4904-9077-71c5fa2eca20', N'0f8f4d5b-d9cb-469f-a165-74267728950e')
/****** Object:  Table [dbo].[Borrows]    Script Date: 01/27/2016 14:06:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Borrows](
	[CopiesID] [uniqueidentifier] NOT NULL,
	[BorrowDate] [datetime] NOT NULL,
	[UserID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Borrows] PRIMARY KEY CLUSTERED 
(
	[CopiesID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Borrows] ([CopiesID], [BorrowDate], [UserID]) VALUES (N'555e6689-7425-40de-944b-e07fc1f90ae7', CAST(0x00009E0B00000000 AS DateTime), N'1b881899-426f-470a-8cef-26e3cc48860b')
/****** Object:  ForeignKey [FK_Books_Authors]    Script Date: 01/27/2016 14:06:50 ******/
ALTER TABLE [dbo].[Books]  WITH CHECK ADD  CONSTRAINT [FK_Books_Authors] FOREIGN KEY([AuthorID])
REFERENCES [dbo].[Authors] ([ID])
GO
ALTER TABLE [dbo].[Books] CHECK CONSTRAINT [FK_Books_Authors]
GO
/****** Object:  ForeignKey [FK_Books_Items]    Script Date: 01/27/2016 14:06:50 ******/
ALTER TABLE [dbo].[Books]  WITH CHECK ADD  CONSTRAINT [FK_Books_Items] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Items] ([ID])
GO
ALTER TABLE [dbo].[Books] CHECK CONSTRAINT [FK_Books_Items]
GO
/****** Object:  ForeignKey [FK_Articles_Authors]    Script Date: 01/27/2016 14:06:50 ******/
ALTER TABLE [dbo].[Articles]  WITH CHECK ADD  CONSTRAINT [FK_Articles_Authors] FOREIGN KEY([AuthorID])
REFERENCES [dbo].[Authors] ([ID])
GO
ALTER TABLE [dbo].[Articles] CHECK CONSTRAINT [FK_Articles_Authors]
GO
/****** Object:  ForeignKey [FK_Articles_Items]    Script Date: 01/27/2016 14:06:50 ******/
ALTER TABLE [dbo].[Articles]  WITH CHECK ADD  CONSTRAINT [FK_Articles_Items] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Items] ([ID])
GO
ALTER TABLE [dbo].[Articles] CHECK CONSTRAINT [FK_Articles_Items]
GO
/****** Object:  ForeignKey [FK_Copies_Items]    Script Date: 01/27/2016 14:06:50 ******/
ALTER TABLE [dbo].[Copies]  WITH CHECK ADD  CONSTRAINT [FK_Copies_Items] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Items] ([ID])
GO
ALTER TABLE [dbo].[Copies] CHECK CONSTRAINT [FK_Copies_Items]
GO
/****** Object:  ForeignKey [FK_Magazines_Items]    Script Date: 01/27/2016 14:06:50 ******/
ALTER TABLE [dbo].[Magazines]  WITH CHECK ADD  CONSTRAINT [FK_Magazines_Items] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Items] ([ID])
GO
ALTER TABLE [dbo].[Magazines] CHECK CONSTRAINT [FK_Magazines_Items]
GO
/****** Object:  ForeignKey [FK_ArticlesInMagazines_Articles]    Script Date: 01/27/2016 14:06:50 ******/
ALTER TABLE [dbo].[ArticlesInMagazines]  WITH CHECK ADD  CONSTRAINT [FK_ArticlesInMagazines_Articles] FOREIGN KEY([ArticleID])
REFERENCES [dbo].[Articles] ([ItemID])
GO
ALTER TABLE [dbo].[ArticlesInMagazines] CHECK CONSTRAINT [FK_ArticlesInMagazines_Articles]
GO
/****** Object:  ForeignKey [FK_ArticlesInMagazines_Magazines]    Script Date: 01/27/2016 14:06:50 ******/
ALTER TABLE [dbo].[ArticlesInMagazines]  WITH CHECK ADD  CONSTRAINT [FK_ArticlesInMagazines_Magazines] FOREIGN KEY([MagazineID])
REFERENCES [dbo].[Magazines] ([ItemID])
GO
ALTER TABLE [dbo].[ArticlesInMagazines] CHECK CONSTRAINT [FK_ArticlesInMagazines_Magazines]
GO
/****** Object:  ForeignKey [FK_Borrows_Copies]    Script Date: 01/27/2016 14:06:50 ******/
ALTER TABLE [dbo].[Borrows]  WITH CHECK ADD  CONSTRAINT [FK_Borrows_Copies] FOREIGN KEY([CopiesID])
REFERENCES [dbo].[Copies] ([ID])
GO
ALTER TABLE [dbo].[Borrows] CHECK CONSTRAINT [FK_Borrows_Copies]
GO
/****** Object:  ForeignKey [FK_Borrows_Users]    Script Date: 01/27/2016 14:06:50 ******/
ALTER TABLE [dbo].[Borrows]  WITH CHECK ADD  CONSTRAINT [FK_Borrows_Users] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([ID])
GO
ALTER TABLE [dbo].[Borrows] CHECK CONSTRAINT [FK_Borrows_Users]
GO
