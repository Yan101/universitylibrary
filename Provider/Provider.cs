﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Common;//Для получения ссылки на UniversityDATA_ADAPTER
using Common.UniversityLibruryDataSetTableAdapters;
using System.Data;  //Для Адаптера
using System.Data.SqlClient;    //Клиент для адаптера

namespace Provider
{
    //Непосредственное общение с источником Данных
    //практически уровень DataSet
    //Запрашивает тип источника данных (connString / XML-файл)
    //Будет получать соединение с источником данных, независимо от того какой это источник данных и где он находится,
    //извлекать из него всю информацию и передавать выше на уровень связанный с провайдером!
    public class Provider
    {
        //1 получили ссылку на Адаптер
        UniversityLibruryDataSet libruryDataSet = new UniversityLibruryDataSet();
        
        //2 Создать адаптер. Лучше юзать свой Адаптер, но при этом юзать типизированынй DataSet.
        SqlDataAdapter[] libruryDataAdapters;
        
        //4.11 Все это для того чтобы пройтись  циклом и извлечь таблицы FILL()
        DataTable[] libruryTables;
        string[] TablesNames;

        //Настрока подключения
        SourceType dataType;    //Тип подключения БД
        string targetFile;  //XML или Sql server
        public Provider()   {   }
        
        //3 Провайдер должен возвращать данные полученные из источника данных.
        //3.1 Извлечение данных и возврат их.  ПОЛУЧАЕТ данные ДАТАСЕТ, заполняет его данными И ВОЗВРАЩАЕТ
        public UniversityLibruryDataSet GetAllData(SourceType dataType, string targetFile) //путь к файлу и ТИП БД
        {
            this.dataType = dataType;
            this.targetFile = targetFile;

            if (this.dataType == SourceType.XML)
            {
                this.libruryDataSet.ReadXml(targetFile);
                return this.libruryDataSet;
            }

            this.libruryTables = new DataTable[]    //Массив состоит из таблиц
            {
                this.libruryDataSet.Items, //ItemsDataSet - типизированный датаСет через свойство можно и по другому
                this.libruryDataSet.Authors,
                this.libruryDataSet.Books,
                this.libruryDataSet.Borrows,
                this.libruryDataSet.Copies,
                this.libruryDataSet.Magazines,
                this.libruryDataSet.Articles,
                this.libruryDataSet.ArticlesInMagazines,
                this.libruryDataSet.Users
            };

            //Создать название таблиц
            this.TablesNames = new string[] 
            {
                "Items", "Authors", "Books", 
                "Borrows", "Copies", "Magazines", 
                "Articles", "ArticlesInMagazines", "Users"
            };

            this.libruryDataAdapters = new SqlDataAdapter[this.TablesNames.Length];
            SqlConnection con = new SqlConnection(this.targetFile);

            //Пройтись по массиву libruryDataAdapters и загнать в него DataAdapter'ы
            for (int i = 0; i < this.TablesNames.Length; i++)
            {
                this.libruryDataAdapters[i] = new SqlDataAdapter("SELECT * FROM  " + this.TablesNames[i], con); //Из массива достаем имя таблицы и подставляем его!

                //для создания свойств UpdateCommand, insert Delete .. .Command
                SqlCommandBuilder combld = new SqlCommandBuilder(this.libruryDataAdapters[i]);
            }

            //Получить данные - Загрузить Данные в DataSet!
            //Fill - загрузка соответствующей таблицы данного DataSet.
            for (int i = 0; i < this.libruryDataAdapters.Length; i++)
                libruryDataAdapters[i].Fill(this.libruryTables[i]); //каждому адаптеру будет предоставленна его личная таблицы

            this.libruryDataSet.WriteXml("BookLibrury.xml");
            this.libruryDataSet.WriteXmlSchema("BookLibrurySchema.xsd");

            return this.libruryDataSet; //отдаем куда-то наверх заполненный типизированный DataSet
        }
        
        //Update - будет обновлять сразу весь DataSet. взамену UpdateArticle.. Add.. Delete.. Этот метод проще
        public void UpdateAllData()
        {
            if (this.dataType == SourceType.XML)
            {
                this.libruryDataSet.WriteXml(this.targetFile);
                return;
            }

            //Сохраняем через Адаптеры, если не XMl. Проходимся по всем адаптерам и передаем каждому адаптеру в метод Update соответствующую ему таблицу! (мы исп-ем свои адаптеры!)
            for (int i = 0; i < this.libruryDataAdapters.Length; i++)
                this.libruryDataAdapters[i].Update(this.libruryTables[i]);
        }
    }
}
