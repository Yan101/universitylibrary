﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Common;


namespace AccessToDataLibrury
{
    public partial class AccessToDataLibrury
    {
        public List<User> GetAllUsers()
        {
            List<User> users = new List<User>();
            DataRowCollection allUsers = libruryDataSet.Users.Rows;

            foreach (UniversityLibruryDataSet.UsersRow userRow in allUsers)
                users.Add(new User(userRow.ID, userRow.Name, userRow.UserName, userRow.Password, userRow.IsAdmin));

            return users;
        } 

        public List<BorrowedCopie> GetAllBorrowedCopiesByUser(Guid userID)
        {
            List<BorrowedCopie> allBorrowedCopieByUser = new List<BorrowedCopie>();

            DataRow[] borrowedCopieByUser = libruryDataSet.Borrows.Select("UserID = '" + userID + "'");
            foreach (UniversityLibruryDataSet.BorrowsRow borRow in borrowedCopieByUser)
            {
                allBorrowedCopieByUser.Add(new BorrowedCopie(borRow.CopiesID, borRow.UserID, borRow.BorrowDate, borRow.CopiesRow.ItemsRow.Name));
            }
            return allBorrowedCopieByUser;
        }

        public bool ReturnBooks(List<Guid> retunedItem)
        {
            foreach (Guid copieID in retunedItem)
            {
                try
                {
                    libruryDataSet.Borrows.FindByCopiesID(copieID).Delete();
                }
                catch
                {

                }
            }
            provider.UpdateAllData();
            return true;
        }

        public bool AddUser(User user)
        {
            try
            {
                libruryDataSet.Users.AddUsersRow(user.ID, user.Name, user.UserName, user.Password, user.IsAdmin);
                provider.UpdateAllData();
            }
            catch
            {
                return false;
            }
            return true;
        }

        public User CheckUser(string userName, Guid password)
        {
            UniversityLibruryDataSet.UsersRow[] userRows =
                (UniversityLibruryDataSet.UsersRow[]) libruryDataSet.Users.Select("UserName ='" + userName +
                                                                                  "' AND Password = '" + password + "'");

            if (userRows != null && userRows.Length > 0)
                return new User(userRows[0].ID, userRows[0].Name, userRows[0].UserName, userRows[0].Password, userRows[0].IsAdmin);
            else
                return null;
        }
    }
}
