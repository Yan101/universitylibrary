﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Common;

namespace AccessToDataLibrury
{
    public partial class AccessToDataLibrury
    {
        public List<Magazine> GetAllMagazines()
        {
            var searchedRow = libruryDataSet.Magazines.Select();
            return GetRestPartsOfMagazines(searchedRow);
        }

        public List<Magazine> GetSearchedMagazines(Magazine magazine)
        {
            UniversityLibruryDataSet.ItemsRow[] searchedItemsRows =
                (UniversityLibruryDataSet.ItemsRow[])
                    libruryDataSet.Items.Select(MakeFilteredQuery(magazine.ItemFields));

            if (searchedItemsRows == null || searchedItemsRows.Length == 0)
                return new List<Magazine>(0);

            // 1 из 3 типизированный список для ItemsRow
            List<UniversityLibruryDataSet.ItemsRow> searchedItemsRowsList =
                searchedItemsRows.ToList<UniversityLibruryDataSet.ItemsRow>();

            // 2 из 3 типизированный список для MagazinesRow
            List<UniversityLibruryDataSet.MagazinesRow> seachedMagazinesRowsList =
                new List<UniversityLibruryDataSet.MagazinesRow>();

            // 1 пройтись по searchedItemsRowsList
            foreach (UniversityLibruryDataSet.ItemsRow itemRow in searchedItemsRowsList)
            {
                UniversityLibruryDataSet.MagazinesRow[] magRows = itemRow.GetMagazinesRows();

                if (magRows != null && magRows.Length > 0) //вдруг нулевой? 
                    seachedMagazinesRowsList.Add(magRows[0]);
            }
            //Получили все строки без учета IssueNumber, теперь убрать лишние IssueNumber которые не указаны в поиске
            List<UniversityLibruryDataSet.MagazinesRow> readMagazinesRows =
                seachedMagazinesRowsList; //некоторые записи тут будут

            //Условие фильтрации проверяется, если поле IssueNumber заполнено!!!
            //Если readMagazinesRows пришло пустым. то все равно поместить в него строки
            if (magazine.IssueNumber != string.Empty) //вычитаем лишние записи
                readMagazinesRows = seachedMagazinesRowsList.FindAll(val => val.IssueNumber == magazine.IssueNumber);

            if (readMagazinesRows == null || readMagazinesRows.Count == 0)
                return new List<Magazine>(0);

            return GetRestPartsOfMagazines(readMagazinesRows.ToArray());
                // получили данные и их надо дополнить из таблицы Items
        }

        private List<Magazine> GetRestPartsOfMagazines(DataRow[] searchedRow)
        {
            if (searchedRow == null || searchedRow.Length == 0)
                //Двойное условие, сначала проверится первое, если false, то тогда проверится второе, но не наоборот, иначе ошибка!!!
                return null;

            //если не null, то создаем список Журналов и будем их наполнять
            List<Magazine> magazines = new List<Magazine>();
            foreach (UniversityLibruryDataSet.MagazinesRow magazineRow in searchedRow)
            {
                //единичный объект магазин, чтобы наполнять св-ва, потом добавлять ег ов общий список магазинов
                var magazine = new Magazine();
                magazine.IssueNumber = magazineRow.IssueNumber;

                //получаем соответствующую Родительскую строку из таблицы Items
                if (magazineRow.ItemsRow != null) //если есть такая строка, то...Целостность данных. Если запись есть в таблице magazines, но отсутствует в таблице Items, то несоответствие таблиц
                {
                    magazine.ID = magazineRow.ItemsRow.ID;
                    magazine.Name = magazineRow.ItemsRow.Name;
                    magazine.Publicher = magazineRow.ItemsRow.Publicher;
                    magazine.PublichedDate = magazineRow.ItemsRow.PublichedDate;
                }
                else
                {
                    throw new Exception("Cannot find corresponding Item");
                }

                magazines.Add(magazine); //Если все ОК, то добавим Магазин!
            }
            return magazines;
        }

        public List<Magazine> AddMagazine(List<Magazine> magazines) //пройтись по спискам Журналов в Бд, и если нету такого журнала, то его нужно добавить!
        {
            List<Magazine> notUpdatedMag = new List<Magazine>();
            foreach (Magazine magazine in magazines)
            {
                //Проверить можно ли хотя бы добавлять новый журнал в DAtaSet. Нужно проверить, есть ли такой дурнал в Dataset!!!
                bool canAdd = IsUniqueMagazineInDB(magazine);
                if (!canAdd)
                {
                    notUpdatedMag.Add(magazine); //Если нельзя добавлять, тогда добавить в Форму "Недобавленные) и пропустить выполнение функции!
                    continue;
                }

                //1 создали строку в таблице Items, получили ссылку и
                UniversityLibruryDataSet.ItemsRow itemsRow= libruryDataSet.Items.AddItemsRow(Guid.NewGuid(), magazine.Name,
                    magazine.Publicher, magazine.PublichedDate);

                //2 и используя эту ссылку, создали строку в таблице Magazines.
                libruryDataSet.Magazines.AddMagazinesRow(itemsRow, magazine.IssueNumber);
            }
            provider.UpdateAllData();
            return notUpdatedMag;
        }

        private bool IsUniqueMagazineInDB(Magazine magazine) //Принимает и сравнивает значения полей в табличке Magazines, Items в БД.
        {
            //Изначально ищем по названию журнала, и если номер выпуска не совпадает, то можно добавлять журнал!
            DataRow[] itemsRows = libruryDataSet.Items.Select("Name = '" + magazine.Name + "'");

            if (itemsRows!=null &&itemsRows.Length>0)
            {
                foreach (DataRow itemRow in itemsRows)
                {
                    UniversityLibruryDataSet.MagazinesRow[] magazinesRows =
                        ((UniversityLibruryDataSet.ItemsRow)itemRow).GetMagazinesRows(); //ТОЛЬКО ДЛЯ СТРОГО-ТИПИЗИРОВАННОГО ОБЪЕКТА, МОЖНО ВЫЗВАТЬ СТРОГО-ТИПИЗИРОВАННЫЙ МЕТОД GetMagazinesRows

                    foreach (UniversityLibruryDataSet.MagazinesRow magazineRow in magazinesRows)
                        if (magazine.IssueNumber==magazineRow.IssueNumber)
                            return false;
                }
            }
            return true;
        }

        public List<string> GetAllMagazinesNames() //#1
        {
            List<string> magazinesNames = new List<string>();

            //Получаем данные из Magazines, а не из Items.
            DataRowCollection magazinesRows = libruryDataSet.Magazines.Rows; //получили все записи
            foreach (DataRow magazineRow in magazinesRows)
                magazinesNames.Add(((UniversityLibruryDataSet.MagazinesRow)magazineRow).ItemsRow.Name); //берем из Items, но вначале нужно получить ссылку ItemsRow - на дочернюю запись, предварительно привести к типу!!

            return magazinesNames.Distinct().ToList<string>();
                //все названия журналов, и они будут повторятся, т.к. один журнал - много статей. Distinct() - убрать дубляжи
        }

        public List<string> GetCurrentMagazineAllIssueNumber(string magazineName) //#2
        {
            //Ищем по названию журнала
            List<string> magazineIssueNumbers = new List<string>();
            UniversityLibruryDataSet.ItemsRow[] itemsRows =
                (UniversityLibruryDataSet.ItemsRow[])libruryDataSet.Items.Select("Name = '" + magazineName + "'");  //Select - вернет DataRow[] - поэтому нужно привести тип!
            
            //Получили запись, теперь пройтись по ним и вытянуть IssueNumber и добавить в список!
            foreach (UniversityLibruryDataSet.ItemsRow itemRow in itemsRows)
                magazineIssueNumbers.Add(itemRow.GetMagazinesRows()[0].IssueNumber);  //GetMagazinesRows - В Items берем некую строку и для нее получаем массив дочерних записей magazines
            
            return magazineIssueNumbers;
        }
    }
}