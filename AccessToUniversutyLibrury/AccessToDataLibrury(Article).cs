﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using Common;

namespace AccessToDataLibrury
{
    public partial class AccessToDataLibrury
    {
        public List<Article> GetAllArticles()
        {
            DataRow[] searchedRows = libruryDataSet.Articles.Select();
            return GetRestPartsOfArticles(searchedRows);
        }

        List<Article> GetRestPartsOfArticles(DataRow[] searchedRows)
        {
            if (searchedRows == null || searchedRows.Length == 0)
                return null;

            List<Article> articles = new List<Article>();

            foreach (UniversityLibruryDataSet.ArticlesRow articleRow in searchedRows)
            {
                Article article = new Article();

                //Единственная запись которая есть в таблице, остальные - получаем!
                article.Version = articleRow.Version;

                if (articleRow.ItemsRow != null)
                {
                    article.ID = articleRow.ItemsRow.ID;
                    article.Name = articleRow.ItemsRow.Name;
                    article.Publicher = articleRow.ItemsRow.Publicher;
                    article.PublichedDate = articleRow.ItemsRow.PublichedDate;
                }
                else
                    throw new Exception("Cannot find corresponding item.1");

                if (articleRow.AuthorsRow != null)
                    article.AuthorName = articleRow.AuthorsRow.Name;
                else
                    throw new Exception("Cannot find corresponding item.2");

                //Т.к. статьи связаны с Журналами, то для каждой стать нужно получить название Журнала(Items) и Номер Журнала(Magazines) -> исп-ть связи Items=Art   Art=ArtInMAgazines
                UniversityLibruryDataSet.ArticlesInMagazinesRow[] artInMagRows =
                    articleRow.GetArticlesInMagazinesRows(); //получим записи из таблицы по связям ArtInMAgazines

                //Получили записи из таблицы ArtInMAgazines, теперь пробегаемся по ним!
                if (artInMagRows != null && artInMagRows.Length > 0) //Проверка, есть ли статьи
                {
                    foreach (UniversityLibruryDataSet.ArticlesInMagazinesRow artInMagRow in artInMagRows)
                    {
                        //Номер журнала в котором опубликована статья!
                        article.MagazineIssueNumber = artInMagRow.MagazinesRow.IssueNumber;
                        //Навазние статьи из таблицы Items!
                        article.MagazineName = artInMagRow.MagazinesRow.ItemsRow.Name;
                    }
                }
                    /*
                else
                    throw new Exception("Cannot find corresponding item.3");
                */
                //Повытягивали из разных таблиц свойства и теперь добавили нашу статью в коллекцию!
                articles.Add(article);
            }
            return articles;
        }

        public List<Article> GetSearchedArticles(Article searchedArticle)
        {
           //#1 Получить ItemRows.
            UniversityLibruryDataSet.ItemsRow[] searchedItemsRows =
                (UniversityLibruryDataSet.ItemsRow[])libruryDataSet.Items.Select(MakeFilteredQuery(searchedArticle.ItemFields));

            //#2 выполнить select для Authors
            UniversityLibruryDataSet.AuthorsRow[] searchedAuthorsRows =
                (UniversityLibruryDataSet.AuthorsRow[])libruryDataSet.Authors.Select(MakeFilteredQuery(searchedArticle.AuthorFields));

            //Получили наборы данных из двух таблиц! 
            //Теперь проверить на наличие записей! Чтобы было что выводить, если есть записи в родительских таблицах.
            if (searchedItemsRows == null || searchedItemsRows.Length == 0 ||
                searchedAuthorsRows == null || searchedAuthorsRows.Length == 0)
            {
                return new List<Article>(0);
            }

            //Если прошли условие, значит данные связаны и записи есть и мы получили набор дочерних строк!
            //#3 Создадим 2 типизированных списка!
            List<UniversityLibruryDataSet.ItemsRow> searchedItemsRowsList =
                searchedItemsRows.ToList<UniversityLibruryDataSet.ItemsRow>();

            List<UniversityLibruryDataSet.ArticlesRow> searchArticlesRowsList=
                new List<UniversityLibruryDataSet.ArticlesRow>();

            //#4 процесс прохода по searchedItemsRowsList и получения для каждой из строк GetArticlesRow
            //получить соответствующие записи в таблице Article для каждой из строк из списка searchedItemsRowsList
            foreach (UniversityLibruryDataSet.ItemsRow itemRow in searchedItemsRowsList)
            {
                UniversityLibruryDataSet.ArticlesRow[] articlesRows = itemRow.GetArticlesRows();
                
                //если массив содержит хотя бы 1 эл-т, то нужно получить этот элемент
                if (articlesRows != null && articlesRows.Length > 0)
                    searchArticlesRowsList.Add(articlesRows[0]);
            }

            //#5 создадим последний список ArticlesRow! его будем лепить из данного searchArticlesRowsList
            List<UniversityLibruryDataSet.ArticlesRow> realArticlesRows =
                new List<UniversityLibruryDataSet.ArticlesRow>();

            //Выбрать все статьи, написанные конкретным автором. Нужно пройтись по searchedAuthorsRows и сравнить их ID на предмет того, какой ID был передан через наш объект Article.
            for (int i = 0; i < searchedAuthorsRows.Length; i++)
            {
                //создали временный лист, в котором после FindAll будут хранится авторы, хранящиеся в массиве searchedAuthorsRows.
                List<UniversityLibruryDataSet.ArticlesRow> tempSearched =
                    searchArticlesRowsList.FindAll(val => val.AuthorID == searchedAuthorsRows[i].ID); 

                realArticlesRows.AddRange(tempSearched);
            }

            //последняя проверка
            if (realArticlesRows == null || realArticlesRows.Count == 0)
                return new List<Article>(0);

            return GetRestPartsOfArticles(realArticlesRows.ToArray());
        }


        public void AddArticle(List<Article> article_)
        {
            //DataRow[] magazineRow   и  AuthorRow.
            //1 для каждой статьи должна быть ссылка на существующий журнал, т.к. добавить статью в несущ урнал - невозможно,
            //каждый журнал ассоциируется с номером (выпуском) этого журнала, поэтому для каждой статьи должна быть ссылка на опр. номер, опр. журнала 
            //и должна быть ссылка на автора. 
            //Если журнала -нет, то добавлять статью нельзя. Если есть номер журнала и в этом журнале нету такой же статьи, с таким же названием, такого же автора, тогда можно добавить статью, иначе - нет.
            //Если у вставляемой статьи нету автора, тогда создаем нового автора, а если автор есть, то получить на него ссылку.
            
            //Перед добавление статьи - необходимо получить ссылку на номер журнала
            DataRow magazineRow, authorRow;

            //Проверить на уникальность каждую добавляему статью
            foreach (Article article in article_)
            {
                bool canAdd = IsUniqueArticleInDB(article, out magazineRow, out authorRow); //уникальность( передать статью, получить ссылку на номер журнала, получить ссылку на запись о авторе)
                if (!canAdd)
                    continue;

                //проверяем наличие автора в БД
                UniversityLibruryDataSet.AuthorsRow authorRow_;
                if (authorRow == null) //1 Если автора нету, то создаем нового автора!
                    authorRow_ = libruryDataSet.Authors.AddAuthorsRow(Guid.NewGuid(), article.AuthorName);
                else
                    authorRow_ = (UniversityLibruryDataSet.AuthorsRow) authorRow; //1 просто добавим сущ автора

                UniversityLibruryDataSet.ItemsRow itemRow =
                    libruryDataSet.Items.AddItemsRow(Guid.NewGuid(), article.Name, article.Publicher, article.PublichedDate); //2 добавили новую строку со статьей в Items!

                UniversityLibruryDataSet.ArticlesRow addedArticleRow =
                    libruryDataSet.Articles.AddArticlesRow(itemRow, authorRow_, article.Version); //3 создали запись в таблице Articles!

                libruryDataSet.ArticlesInMagazines.AddArticlesInMagazinesRow(addedArticleRow,
                    (UniversityLibruryDataSet.MagazinesRow)magazineRow);   //4 Создали запись в таблтце ArticlesInMagazines
            }
            provider.UpdateAllData();
        }

        private bool IsUniqueArticleInDB(Article article, out DataRow magazineRow, out DataRow authorRow)
        {
            magazineRow = null;
            authorRow = null;

            //проверяем существование в БД автора данной статьи
            UniversityLibruryDataSet.AuthorsRow[] authorRows =
                (UniversityLibruryDataSet.AuthorsRow[])libruryDataSet.Authors.Select("Name = '" + article.AuthorName + "'");

            if (authorRows != null && authorRows.Length > 0)
                authorRow = authorRows[0];
            
            //Проверяем в данном номере журнала существования добавляемой статьи.
            //Берем из таблицы Items, журналы с данным Названием! т.е. все выпуски данного журнала
            UniversityLibruryDataSet.ItemsRow[] itemsRows =
                (UniversityLibruryDataSet.ItemsRow[])libruryDataSet.Items.Select("Name = '" + article.MagazineName + "'");
            
            //Если нет журнала, то куда мы пытаемся добавить статью?
            if (itemsRows == null && itemsRows.Length == 0)
                return false; //Сама по себе статья существовать не может, буз журнала

            foreach (UniversityLibruryDataSet.ItemsRow itemRow in itemsRows)
            {
                //Данные из Magazines
                UniversityLibruryDataSet.MagazinesRow[] magazineRows = itemRow.GetMagazinesRows();

                if (magazineRows == null || magazineRows.Length == 0)
                    throw new Exception("Cannot find corresponding item.");

                //получили записи, но нужна только первая
                //Если номер журнала не тот, то нет смысла дальше его изучать...
                //Если IssueNum(номер журнала) не совпадает с полем IssueNum для добавляемой стать, это значит что этот журнал нас не интересует
                if (magazineRows[0].IssueNumber != article.MagazineIssueNumber)
                    continue; //пропуск

                //Если данный номер данного журнала все-таки существует, то мы можно добавить статью в журнал, если такой стать в данном номере данного журнала еще нет!
                magazineRow = magazineRows[0]; //получили ссылку на строку требуемого номера журнала.

                //получить ссылку на все статьи данного журнала.
                UniversityLibruryDataSet.ArticlesInMagazinesRow[] artinMagRows =
                    magazineRows[0].GetArticlesInMagazinesRows();

                //перебираем все статьи в данном номере журнала и проверяем их названия!
                foreach (UniversityLibruryDataSet.ArticlesInMagazinesRow artInMagRow in artinMagRows)
                    if (article.Name == artInMagRow.ArticlesRow.ItemsRow.Name) //если название добавляемой статьи совпадает с названием одной из существующих статей в данном номере журнала, тгда false
                        return false;

                return true;
            }
            return false; //
        }
    }
}