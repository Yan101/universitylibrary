﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using Common;

namespace AccessToDataLibrury
{
    public partial class AccessToDataLibrury
    {
        public List<Book> GetAllBooks()
        {
            //получить из объекта DataSet - список книг! (залезть в DS в таблице Books, и получить данные, сформировав при этом объекты Book и занести их в список List<Book> с помощью св-ва Rows!!!
            //инфа о книге разбита по трем таблицам! Items Books Autors 
            //получаем DataRowCollection - т.е. получить все строки из таблицы Books с помощью св-ва Rows.
            //DataRowCollection searchedRow = this.libruryDataSet.Books.Rows;
            
            //ВТОРОЙ ПАРИАНТ! исп-ем Select() для Books
            DataRow[] searchedRow = this.libruryDataSet.Books.Select(); //просто получим метод который выберем все строки List<Book>


            //испольщуюя все полученные строки, передаем их методу GetRestPartsOfBook и для каждой записи из передаваемой коллекции достаем инфу(Из самой коллекции достаем ISBN, а через св-ва ItemsRow и AothorsRow - получаем доступ к соответствующим связанным строкам из Родительских таблиц!!!
            return this.GetRestPartsOfBook(searchedRow);

        }

        //получить список DataRowCollection, который будет содержать строки из таблицы Books, но инфа не полная, поэтому исп-я строки из Books нужно получить недостающую инфу из двух остальных таблицы (Authors, Items)
        //Этот метод возвращает недостающий значения из соответствующих таблиц для книги! Items, Author
        private List<Book> GetRestPartsOfBook(DataRowCollection searchedRows) //DataRowCollection - содержит строки относящиеся к таблице Books
        {
            if (searchedRows == null || searchedRows.Count == 0)
                return null;

            List<Book> books = new List<Book>();
            foreach (UniversityLibruryDataSet.BooksRow bookRow in searchedRows) //Строго типизированная переменная BooksRow, возвращающая типизированная строку из таблицы Books. BooksRow - класс-обертка, для таблицы books
            {
                Book book = new Book(); //Создали новую книгу
                book.ISBN = bookRow.ISBN;

                //т.к. есть связь между таблицей Items и таблицей Books 1 к 1, то исп-я эту зависимость, можно перемещатся между двумя таблицами

                if (bookRow.ItemsRow != null) //данные из таблицы Items
                {
                    book.ID = bookRow.ItemsRow.ID; //одинаковые значения
                    book.Name = bookRow.ItemsRow.Name;
                    book.Publicher = bookRow.ItemsRow.Publicher;
                    book.PublichedDate = bookRow.ItemsRow.PublichedDate;

                }
                else
                    throw new Exception("Cannot find corresonding Item");

                if (bookRow.AuthorsRow != null) //данные из таблицы Author
                {
                    book.AuthorName = bookRow.AuthorsRow.Name;

                }
                else
                    throw new Exception("Cannot find corresonding Author");

                //Дабавить данную книгу в список книг!
                books.Add(book);
            }
            return books;
        }
        private List<Book> GetRestPartsOfBook(DataRow[] searchedRows) //ВТОРОЙ ПАРИАНТ! //DataRowCollection - содержит строки относящиеся к таблице Books
        {
            if (searchedRows == null || searchedRows.Length == 0)
                return null;

            //Если searchedRows не НОЛЬ и Есть какие то записи в в таблице Books - создать список List
            List<Book> books = new List<Book>();
            foreach (UniversityLibruryDataSet.BooksRow bookRow in searchedRows)
            {
                Book book = new Book();
                book.ISBN = bookRow.ISBN;

                /*
                DataRow[] itemRows = this.libruryDataSet.Items.Select("ID = " + bookRow.ItemID); //связь 1к1, Ищем в таблице Items. Пригодилась строка bookRow(из нее извлекаем ItemID) ис ним сравниваем значение. В итоге получим значение из баблицы Items
                if (itemRows != null && itemRows.Length != 0)
                {
                    UniversityLibruryDataSet.ItemsRow itemRow = (UniversityLibruryDataSet.ItemsRow)itemRows[0];
                    book.ID = bookRow.ItemsRow.ID; //одинаковые значения
                    book.Name = bookRow.ItemsRow.Name;
                    book.Publicher = bookRow.ItemsRow.Publicher;
                    book.PublichedDate = bookRow.ItemsRow.PublichedDate;
                }
                */
                if (bookRow.ItemsRow != null)
                {
                    book.ID = bookRow.ItemsRow.ID; //одинаковые значения
                    book.Name = bookRow.ItemsRow.Name;
                    book.Publicher = bookRow.ItemsRow.Publicher;
                    book.PublichedDate = bookRow.ItemsRow.PublichedDate;
                }
                else
                    throw new Exception("Cannot find corresonding Item");

                if (bookRow.AuthorsRow != null) //данные из таблицы Author
                {
                    book.AuthorName = bookRow.AuthorsRow.Name;

                }
                else
                    throw new Exception("Cannot find corresonding Author");
                books.Add(book);
            }
            return books;
        }

        public List<Book> GetSearchedBooks(Book searchedBook)
        {
            //#2 

            List<Book> books = new List<Book>();
            
            //ищем по таблице Books, по полям Publicher, PublicherDate... - этих полей нету в таблице, но Select работает.
            string filter = "Parent(FK_Books_Items).Name Like '%" + searchedBook.Name +
                            "%' AND Parent(FK_Books_Authors).Name Like '%" + searchedBook.AuthorName +
                            "%' AND Parent(FK_Books_Items).Publicher Like '%" + searchedBook.Publicher +
                            "%' AND Convert(Parent(FK_Books_Items).PublichedDate, 'System.String') Like '%" + searchedBook.PublichedDate +
                            "%'"; //получим доступ к Родительской таблице!
            
            DataRow[] searchedBooks = libruryDataSet.Books.Select(filter);
            /*
            foreach (DataRow book in searchedBooks)
            {
                books.Add(new Book(book.GetParentRow("FK_Books_Items")["Name"].ToString(),
                    book.GetParentRow("FK_Books_Items")["Publicher"].ToString(),
                    book.GetParentRow("FK_Books_Items")["PublichedDate"].ToString(),
                    book["ISBN"].ToString(),
                    book.GetParentRow("FK_Books_Authors")["Name"].ToString()));
            }
            return books;
            */

            //#2.1 исп-ем приведение!!! 
            foreach (UniversityLibruryDataSet.BooksRow book in searchedBooks)
            {
                books.Add(new Book(
                    book.ItemsRow.Name,
                    book.ItemsRow.Publicher,
                    book.ItemsRow.PublichedDate,
                    book.ISBN,
                    book.AuthorsRow.Name));
            }
            return books;

            //#1 ищем по двую таблицам, обьединяем - убираем ненужные..
            /*
            //Поиск по двум таблицам! Получаем набор найденных строк из Items
            //Получить массив строго типизированных DataRow[] (массив ItemsRow[] строготипзированный). Для его получения - исп-ем Select для выборки по критерию, но Select- нетипизированный, поэтому кастим !!!
            UniversityLibruryDataSet.ItemsRow[] searchedItemsRows = 
                (UniversityLibruryDataSet.ItemsRow[])libruryDataSet.Items.Select(MakeFilteredQuery(searchedBook.ItemFields));

            //Получаем набор найденных строк из таблицы Authors
            UniversityLibruryDataSet.AuthorsRow[] searchedAuthorsRows =
                (UniversityLibruryDataSet.AuthorsRow[])libruryDataSet.Authors.Select(MakeFilteredQuery(searchedBook.AuthorFields));

            //искать строки в таблице Books не стоит, т.к. поиск по по полю ISBN - не выполняется, а это поле находится в таблице Books!
            //магия поиска 4.23 12.00
            if (
                    (searchedItemsRows == null || searchedItemsRows.Length == 0) || //Books содержит две ссылки на таблицы Items, Authors и если нету полей Items, то не будет и Authors и наоборот.
                    (searchedAuthorsRows == null || searchedAuthorsRows.Length == 0) //или дата издания есть, а автора нет, то и КНИГИ нет!!!
                )
                return new List<Book>(0); //Возвращаем пустуе, но существующие книги

            //создаем списи для набора строк Items и Books для более гибкого манипулирования эл-ми массива.
            List<UniversityLibruryDataSet.ItemsRow> searchedItemsRowsList =
                searchedItemsRows.ToList<UniversityLibruryDataSet.ItemsRow>();

            //
            List<UniversityLibruryDataSet.BooksRow> searchedBooksRowsList =
                new List<UniversityLibruryDataSet.BooksRow>();

            //Заполнить список книг! Для каждой записи в таблице Items, существует всего одна запись в таблице Books
            //Найти все книги, удовлетворяющие поиск, без учета АВТОРА
            foreach (UniversityLibruryDataSet.ItemsRow itemRow in searchedItemsRowsList)
            {
                //Вытащить связанную дочернюю запись из таблицы Books
                UniversityLibruryDataSet.BooksRow[] bookRow = itemRow.GetBooksRows();
                if (bookRow != null && bookRow.Length !=0)
                {
                    searchedBooksRowsList.Add(bookRow[0]);
                }
            }

            //конечный набор искомых строк
            List<UniversityLibruryDataSet.BooksRow> realBooksRows =
                new List<UniversityLibruryDataSet.BooksRow>();

            //выбрать те книги, которые написанны искомым автором из всех книг, которые уже отобраны поиском!
            for (int i = 0; i < searchedAuthorsRows.Length; i++)
            {
                //временные
                List<UniversityLibruryDataSet.BooksRow> tempSearched =              //FindAll - вернет новый список
                    searchedBooksRowsList.FindAll(
                    val => val.AuthorID == searchedAuthorsRows[i].ID); //val представляет каждую строку в списке searchedBooksRowsList. val => (УСЛОВИЕ)
                
                realBooksRows.AddRange(tempSearched);
            }

            if (realBooksRows.Count==0) //Не нашли строк
                return new List<Book>(0);

            return GetRestPartsOfBook(realBooksRows.ToArray());
             * */
        }

        //4.28 Метод принимате список книг, заносит книгу в Dataset и отправляет DataSet на Update в Provider
        public void AddBook(List<Book> book_)
        {
            //для провайдера вызвать метод UpdateAllData, но перед этим нужно изменить DataSet ( добавить новую книгу )
            //Items Books Authors,если автора нет то все три, А Если автор уже есть, то для таблицы Books получить значения ID -Authors не менять!!!

            DataRow authorDataRow; //Если нету автора, создадим его и получим ссылку authorRow, если есть - просто получим ссылку!!!
            foreach (Book book in book_)
            {
                //Проверям уникальность книги и воможность ее добавления! Есть литакая книга в БД?
                //out - если номера книгу нету, но автор уже есть, то надо проверять! 
                bool canAdd = IsUniqueBookInDB(book, out authorDataRow, LibruryAction.AddBook);
                if (!canAdd)
                    continue;

                //Если можно добавлять книгу, то начинаем ее добавлять
                //После того как у нас будут itemRow, AuthorRow - можно исп-ть ссылки на эти строки, на их первичные ключи, чтобы создать новую строку в Book (Book - связана внешними ключами с Items и Authors)
                
                //1. Добавить запись в таблицу ItemsRow(?) т.е. часть книги уже втавили в DataSet
                UniversityLibruryDataSet.ItemsRow itemRow =
                    libruryDataSet.Items.AddItemsRow(Guid.NewGuid(), book.Name, book.Publicher, book.PublichedDate);
                
                //2. Работаем со строкой AuthorRow, но не устанавливаем: IsUniqueBookInDB - определила что книги нету и Автора тоже!
                UniversityLibruryDataSet.AuthorsRow authorRow;

                //если автора нету в БД
                if (authorDataRow == null)
                {
                    //если атвора нету
                    authorRow = libruryDataSet.Authors.AddAuthorsRow(Guid.NewGuid(), book.AuthorName);
                }
                else
                    authorRow = (UniversityLibruryDataSet.AuthorsRow)authorDataRow;

                //Добавляем новую строку в Books. AddBooksRow(строка из таблицы Item которая содержит внешний ключ для записи в таблицу Books, внешний ключ AuthorsRow, isbn из book)
                libruryDataSet.Books.AddBooksRow(itemRow, authorRow, book.ISBN);
            }

            provider.UpdateAllData();
        }

        //4.34 
        public void UpdateBooks(List<Book> book_)
        {
            //1 Идентифицировать книгу: В таблицах Items, Books, Authors, находить именно ту запись, которую мы хотим изменять. Идентифицировать по ID
            //2 Проверить уникальность книги после изменения!
            
            DataRow author;
            foreach (Book book in book_) //Проверяем уникальность, если нет - то дальше!
            {
                bool canUpdate = IsUniqueBookInDB(book, out author, LibruryAction.UpdateBook );
                if (!canUpdate)
                    continue;

                //1 itemRow
                //Если можно изменять данную книгу. Новые данные еще не присутствуют в БД
                UniversityLibruryDataSet.ItemsRow itemRow = libruryDataSet.Items.FindByID(book.ID);

                // Установить параметры ItemRow
                itemRow.Name = book.Name;
                itemRow.Publicher = book.Publicher;
                itemRow.PublichedDate = book.PublichedDate;

                //2 authorRow
                UniversityLibruryDataSet.AuthorsRow authorRow;
                if (author == null) //При изменении книги, если нету такого автора в БД, то добавить его в БД!
                    authorRow = libruryDataSet.Authors.AddAuthorsRow(Guid.NewGuid(), book.AuthorName);
                else
                    authorRow = (UniversityLibruryDataSet.AuthorsRow)author;

                //3 bookRow
                UniversityLibruryDataSet.BooksRow bookRow = libruryDataSet.Books.FindByItemID(book.ID);

                //НАшли 3 строки!! 

                bookRow.ISBN = book.ISBN;
                bookRow.AuthorID = authorRow.ID;
            }
            provider.UpdateAllData();
        }
        private bool IsUniqueBookInDB(Book book, out DataRow authorRow, LibruryAction action) //action - каким образом будет проверять ISBN
        {
            //Вернет максимум 1 эл-т т.к. в Бд может быть только 1 уникальный автор или ни одного!
            DataRow[] bookRows = libruryDataSet.Books.Select("[ISBN] = '" + book.ISBN + "'");

            authorRow = null;

            if (action == LibruryAction.AddBook)
            {
                if (bookRows != null && bookRows.Length > 0) //Такая книга с этим ISBN уже есть в БД! Новую добавлять нельзя
                    return false;
            }
            else
                if (action == LibruryAction.UpdateBook)         //Одновременно проверяет Автора и ISBN! А нам нужно поменять только один ISBN(как один из возможных вариантов!)
                {
                    if (bookRows != null && bookRows.Length > 0) //Если нашли записи с таким же ISBN, нужно проверить ISBN найден в этой же записи (тогда не считается) или в другой, тогда обновлять нельзя!!!
                    {
                        foreach (UniversityLibruryDataSet.BooksRow bookRow in bookRows)
                        {
                           
                            if (bookRow.ItemID != book.ID)
                                return false;
                        }
                    }
                }
                else return false;
            
            //Иначе добавляем книгу!
            //Ищем автора, если нету то получим null
            DataRow[] authorsRows = libruryDataSet.Authors.Select("Name = '" + book.AuthorName + "'");

            //Нашли автора который уже есть в БД!
            if (authorsRows != null && authorsRows.Length > 0)
            {
                authorRow = authorsRows[0]; //Занесли 

                foreach (DataRow authorR in authorsRows) //получаем все книги автора! Чтобы сравнить их названия с тем который предлагается!
                {
                    UniversityLibruryDataSet.BooksRow[] booksRow =
                        ((UniversityLibruryDataSet.AuthorsRow)authorR).GetBooksRows(); //GetBooksRows - получить дочерние книги. Один автор - много книг!

                    foreach (UniversityLibruryDataSet.BooksRow bookR in booksRow) //Получили все книги данного автора и пробегаемся по ним
                    {
                        //Если даннаястрока(которую нашли) не явл-ся исходной, т.е. другая!
                        if (book.ID != bookR.ItemID) //Если строка яв-ся той же которую нашли, то ее проверять не следует, просто сменим ISBN (все и так совпадет). 
                            if (book.Name == bookR.ItemsRow.Name) //если есть совпадения, то книгу нельзя вставлять
                                return false;
                    }
                }
            }
            return true;
        }

        //1 получаем все копии данной книги
        public List<Copie> GetBookCopies(Guid bookID, out string bookName)
        {
            //Получили ID книги, теперь ищем в items имя по ID
            DataRow[] bookCopie = libruryDataSet.Copies.Select("ItemID = '" + bookID + "'");
            bookName = libruryDataSet.Items.FindByID(bookID).Name;

            //список копий данной книги
            List<Copie> copies = new List<Copie>();

            //Данная копия на руках или в библиотеке?
            //Если для данной записи в таблице Copies, сущ доч записи в Borrows - это значит что данная копия находится на руках у студента и ее нету сейчас в библиотеке!
            //Предотвратить выдачу этой копии, возобновить, коглда она появится в библиотеке!
            foreach (UniversityLibruryDataSet.CopiesRow singleCop in bookCopie)
            {
                bool isBorrowed = false;
                //DataRow borrowed = libruryDataSet.Borrows.Rows.Find(singleCop.ID);
                //DataRow[] borrowed = singleCop.GetBorrowsRows();
                DataRow[] borrowed = singleCop.GetChildRows("FK_Borrows_Copies");
                if (borrowed != null && borrowed.Length > 0)
                    isBorrowed = true;

                copies.Add(new Copie(singleCop.ID, singleCop.ItemID, isBorrowed));
            }
            return copies;
        }

        //2 получили все копии, теперь одну нужно отдаь студенту!
        public bool BorrowingBook(List<BorrowedCopie> borCops )
        {
            try
            {
                foreach (BorrowedCopie borCop in borCops)
                {
                    //Добавили строку в Borrows
                    libruryDataSet.Borrows.AddBorrowsRow(libruryDataSet.Copies.FindByID(borCop.CopieID),
                        borCop.BorrowedDate,
                        libruryDataSet.Users.FindByID(borCop.UserID));

                    //Теперь добавить строку в Copies, изменить знач записи IsBorrowed = false, а теперь true.
                    provider.UpdateAllData();
                }
            }
            catch 
            {
                return false;
            }
            return true;
        }
    }
}