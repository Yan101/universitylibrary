﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Common;
using Provider;

namespace AccessToDataLibrury
{
    public partial class AccessToDataLibrury
    {
        UniversityLibruryDataSet libruryDataSet;
        Provider.Provider provider = new Provider.Provider();

        public AccessToDataLibrury(SourceType dataType, string targetData)
        {
            this.libruryDataSet = this.provider.GetAllData(dataType, targetData);
        }

        private string MakeFilteredQuery(Dictionary<string, string> searchedTable)
        {
            string filter = "";

            if (searchedTable == null)
                return filter;

            //foreach т.к. нужно пробегатся по Dictionary : одновременно нужно иметь доступ к ключу и значению.
            foreach (KeyValuePair<string, string> kvp in searchedTable) //KeyValuePair для типа словаря Dictionary
            {
                if (kvp.Value == null || kvp.Value == string.Empty)
                    continue;

                if (string.IsNullOrEmpty(filter))
                    filter += string.Format("[{0}] Like '%{1}%'", kvp.Key, kvp.Value);
                else
                    filter += string.Format("AND [{0}] Like '%{1}%'", kvp.Key, kvp.Value);
            }
            return filter;
        }
    }
}