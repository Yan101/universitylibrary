﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Common;

namespace Librury
{
    public partial class NoAddedItems : Form
    {
        public NoAddedItems(List<Magazine> noAddedMag)
        {
            InitializeComponent();
            notAddedItems_dgv.DataSource = noAddedMag;
        }
    }
}
