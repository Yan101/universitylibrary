﻿namespace Librury
{
    partial class UpdateBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UpdateBook_btn = new System.Windows.Forms.Button();
            this.AuthorName_txbx = new System.Windows.Forms.TextBox();
            this.AuthorName = new System.Windows.Forms.Label();
            this.ISBN_txbx = new System.Windows.Forms.TextBox();
            this.ISBN = new System.Windows.Forms.Label();
            this.PublicherDate_txbx = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Publicher_txbx = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Name_txbx = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ID_txbx = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // UpdateBook_btn
            // 
            this.UpdateBook_btn.Location = new System.Drawing.Point(92, 176);
            this.UpdateBook_btn.Name = "UpdateBook_btn";
            this.UpdateBook_btn.Size = new System.Drawing.Size(165, 23);
            this.UpdateBook_btn.TabIndex = 21;
            this.UpdateBook_btn.Text = "Update Book";
            this.UpdateBook_btn.UseVisualStyleBackColor = true;
            this.UpdateBook_btn.Click += new System.EventHandler(this.UpdateBook_btn_Click);
            // 
            // AuthorName_txbx
            // 
            this.AuthorName_txbx.Location = new System.Drawing.Point(92, 149);
            this.AuthorName_txbx.Name = "AuthorName_txbx";
            this.AuthorName_txbx.Size = new System.Drawing.Size(165, 20);
            this.AuthorName_txbx.TabIndex = 20;
            // 
            // AuthorName
            // 
            this.AuthorName.AutoSize = true;
            this.AuthorName.Location = new System.Drawing.Point(17, 152);
            this.AuthorName.Name = "AuthorName";
            this.AuthorName.Size = new System.Drawing.Size(69, 13);
            this.AuthorName.TabIndex = 19;
            this.AuthorName.Text = "Author Name";
            this.AuthorName.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ISBN_txbx
            // 
            this.ISBN_txbx.Location = new System.Drawing.Point(92, 123);
            this.ISBN_txbx.Name = "ISBN_txbx";
            this.ISBN_txbx.Size = new System.Drawing.Size(165, 20);
            this.ISBN_txbx.TabIndex = 18;
            // 
            // ISBN
            // 
            this.ISBN.AutoSize = true;
            this.ISBN.Location = new System.Drawing.Point(54, 126);
            this.ISBN.Name = "ISBN";
            this.ISBN.Size = new System.Drawing.Size(32, 13);
            this.ISBN.TabIndex = 17;
            this.ISBN.Text = "ISBN";
            this.ISBN.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // PublicherDate_txbx
            // 
            this.PublicherDate_txbx.Location = new System.Drawing.Point(92, 97);
            this.PublicherDate_txbx.Name = "PublicherDate_txbx";
            this.PublicherDate_txbx.Size = new System.Drawing.Size(165, 20);
            this.PublicherDate_txbx.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Publicher Date";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Publicher_txbx
            // 
            this.Publicher_txbx.Location = new System.Drawing.Point(92, 71);
            this.Publicher_txbx.Name = "Publicher_txbx";
            this.Publicher_txbx.Size = new System.Drawing.Size(165, 20);
            this.Publicher_txbx.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Publicher";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Name_txbx
            // 
            this.Name_txbx.Location = new System.Drawing.Point(92, 45);
            this.Name_txbx.Name = "Name_txbx";
            this.Name_txbx.Size = new System.Drawing.Size(165, 20);
            this.Name_txbx.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Title";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(68, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(18, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "ID";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ID_txbx
            // 
            this.ID_txbx.Enabled = false;
            this.ID_txbx.Location = new System.Drawing.Point(92, 19);
            this.ID_txbx.Name = "ID_txbx";
            this.ID_txbx.Size = new System.Drawing.Size(165, 20);
            this.ID_txbx.TabIndex = 12;
            // 
            // UpdateBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(268, 211);
            this.Controls.Add(this.UpdateBook_btn);
            this.Controls.Add(this.AuthorName_txbx);
            this.Controls.Add(this.AuthorName);
            this.Controls.Add(this.ISBN_txbx);
            this.Controls.Add(this.ISBN);
            this.Controls.Add(this.PublicherDate_txbx);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Publicher_txbx);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ID_txbx);
            this.Controls.Add(this.Name_txbx);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Name = "UpdateBook";
            this.Text = "UpdateBook";
            this.Load += new System.EventHandler(this.UpdateBook_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button UpdateBook_btn;
        private System.Windows.Forms.TextBox AuthorName_txbx;
        private System.Windows.Forms.Label AuthorName;
        private System.Windows.Forms.TextBox ISBN_txbx;
        private System.Windows.Forms.Label ISBN;
        private System.Windows.Forms.TextBox PublicherDate_txbx;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Publicher_txbx;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Name_txbx;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ID_txbx;
    }
}