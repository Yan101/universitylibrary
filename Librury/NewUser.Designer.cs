﻿namespace Librury
{
    partial class NewUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.UserName_txbx = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Name_txbx = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Password_txbx = new System.Windows.Forms.TextBox();
            this.newUser_btn = new System.Windows.Forms.Button();
            this.isAdmin_chbx = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "User Name";
            // 
            // UserName_txbx
            // 
            this.UserName_txbx.Location = new System.Drawing.Point(75, 12);
            this.UserName_txbx.Name = "UserName_txbx";
            this.UserName_txbx.Size = new System.Drawing.Size(210, 20);
            this.UserName_txbx.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Name";
            // 
            // Name_txbx
            // 
            this.Name_txbx.Location = new System.Drawing.Point(75, 38);
            this.Name_txbx.Name = "Name_txbx";
            this.Name_txbx.Size = new System.Drawing.Size(210, 20);
            this.Name_txbx.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Password";
            // 
            // Password_txbx
            // 
            this.Password_txbx.Location = new System.Drawing.Point(75, 64);
            this.Password_txbx.Name = "Password_txbx";
            this.Password_txbx.PasswordChar = '*';
            this.Password_txbx.Size = new System.Drawing.Size(210, 20);
            this.Password_txbx.TabIndex = 1;
            // 
            // newUser_btn
            // 
            this.newUser_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.newUser_btn.Location = new System.Drawing.Point(142, 93);
            this.newUser_btn.Name = "newUser_btn";
            this.newUser_btn.Size = new System.Drawing.Size(143, 23);
            this.newUser_btn.TabIndex = 2;
            this.newUser_btn.Text = "Create User";
            this.newUser_btn.UseVisualStyleBackColor = true;
            this.newUser_btn.Click += new System.EventHandler(this.newUser_btn_Click);
            // 
            // isAdmin_chbx
            // 
            this.isAdmin_chbx.AutoSize = true;
            this.isAdmin_chbx.Location = new System.Drawing.Point(75, 97);
            this.isAdmin_chbx.Name = "isAdmin_chbx";
            this.isAdmin_chbx.Size = new System.Drawing.Size(61, 17);
            this.isAdmin_chbx.TabIndex = 3;
            this.isAdmin_chbx.Text = "Admin?";
            this.isAdmin_chbx.UseVisualStyleBackColor = true;
            // 
            // NewUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(296, 128);
            this.Controls.Add(this.isAdmin_chbx);
            this.Controls.Add(this.newUser_btn);
            this.Controls.Add(this.Password_txbx);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Name_txbx);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.UserName_txbx);
            this.Controls.Add(this.label1);
            this.Name = "NewUser";
            this.Text = "NewUser";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox UserName_txbx;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Name_txbx;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Password_txbx;
        private System.Windows.Forms.Button newUser_btn;
        private System.Windows.Forms.CheckBox isAdmin_chbx;
    }
}