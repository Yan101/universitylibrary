﻿namespace Librury
{
    partial class UsersBorrowed
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.allUsers_ltbx = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.allBorrowedCopieByUser_dgv = new System.Windows.Forms.DataGridView();
            this.ReturnItem_btn = new System.Windows.Forms.Button();
            this.copieID_txbx = new System.Windows.Forms.TextBox();
            this.close_btn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.allBorrowedCopieByUser_dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Пользователь: ";
            // 
            // allUsers_ltbx
            // 
            this.allUsers_ltbx.FormattingEnabled = true;
            this.allUsers_ltbx.Location = new System.Drawing.Point(16, 30);
            this.allUsers_ltbx.Name = "allUsers_ltbx";
            this.allUsers_ltbx.Size = new System.Drawing.Size(705, 95);
            this.allUsers_ltbx.TabIndex = 1;
            this.allUsers_ltbx.SelectedIndexChanged += new System.EventHandler(this.allUsers_ltbx_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 145);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "В пользовании: ";
            // 
            // allBorrowedCopieByUser_dgv
            // 
            this.allBorrowedCopieByUser_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.allBorrowedCopieByUser_dgv.Location = new System.Drawing.Point(16, 162);
            this.allBorrowedCopieByUser_dgv.Name = "allBorrowedCopieByUser_dgv";
            this.allBorrowedCopieByUser_dgv.Size = new System.Drawing.Size(705, 187);
            this.allBorrowedCopieByUser_dgv.TabIndex = 2;
            this.allBorrowedCopieByUser_dgv.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.allBorrowedCopieByUser_dgv_CellEnter);
            // 
            // ReturnItem_btn
            // 
            this.ReturnItem_btn.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ReturnItem_btn.ForeColor = System.Drawing.SystemColors.Highlight;
            this.ReturnItem_btn.Location = new System.Drawing.Point(501, 356);
            this.ReturnItem_btn.Name = "ReturnItem_btn";
            this.ReturnItem_btn.Size = new System.Drawing.Size(219, 23);
            this.ReturnItem_btn.TabIndex = 3;
            this.ReturnItem_btn.Text = "Списать";
            this.ReturnItem_btn.UseVisualStyleBackColor = true;
            this.ReturnItem_btn.Click += new System.EventHandler(this.ReturnItem_btn_Click);
            // 
            // copieID_txbx
            // 
            this.copieID_txbx.Location = new System.Drawing.Point(16, 358);
            this.copieID_txbx.Name = "copieID_txbx";
            this.copieID_txbx.ReadOnly = true;
            this.copieID_txbx.Size = new System.Drawing.Size(479, 20);
            this.copieID_txbx.TabIndex = 4;
            // 
            // close_btn
            // 
            this.close_btn.Location = new System.Drawing.Point(16, 385);
            this.close_btn.Name = "close_btn";
            this.close_btn.Size = new System.Drawing.Size(704, 23);
            this.close_btn.TabIndex = 5;
            this.close_btn.Text = "Закрыть";
            this.close_btn.UseVisualStyleBackColor = true;
            this.close_btn.Click += new System.EventHandler(this.close_btn_Click);
            // 
            // UsersBorrowed
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(733, 412);
            this.Controls.Add(this.close_btn);
            this.Controls.Add(this.copieID_txbx);
            this.Controls.Add(this.ReturnItem_btn);
            this.Controls.Add(this.allBorrowedCopieByUser_dgv);
            this.Controls.Add(this.allUsers_ltbx);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "UsersBorrowed";
            this.Text = "UsersBorrowed";
            ((System.ComponentModel.ISupportInitialize)(this.allBorrowedCopieByUser_dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox allUsers_ltbx;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView allBorrowedCopieByUser_dgv;
        private System.Windows.Forms.Button ReturnItem_btn;
        private System.Windows.Forms.TextBox copieID_txbx;
        private System.Windows.Forms.Button close_btn;
    }
}