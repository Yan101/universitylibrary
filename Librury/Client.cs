﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using AccessToDataLibrury;
using Common;
using Provider;

namespace Librury
{
    public partial class Client : Form
    {
        AccessToDataLibrury.AccessToDataLibrury accessToDataLibrury;
        NewArticle newArticle;
        public Client()
        {
            InitializeComponent();

            LogIn logIn = new LogIn();
            if (logIn.ShowDialog() == DialogResult.OK) //Чтобы выполнить привязку к DGV, нужно получить список книг!
            {
                accessToDataLibrury =
                    new AccessToDataLibrury.AccessToDataLibrury(logIn.DBType, logIn.ConnectionParameters);
            }
        }

        private void RefreshBooks_btn_Click(object sender, EventArgs e)
        {
            if (accessToDataLibrury == null)
                return;

            List<Book> allBooks;

            //Выбирать отсортированные строки / поиск по критерию
            if (
                (sender == null && e == null) ||
                //e может быть != null - это возможно, когда мы второй раз нажимаем вручную на кнопку - перепривязать DGV
                (
                    NameBook_txbx.Text.Trim() == string.Empty &&
                    PublicherBook_txbx.Text.Trim() == string.Empty &&
                    AuthorBook_txbx.Text.Trim() == string.Empty
                    )
                )
            {
                allBooks = accessToDataLibrury.GetAllBooks();
                Books_dgv.DataSource = allBooks;
            }
            else //Поиск книг!
            {
                //Создали прототип искомой книги! По этой книге будем искать реальные книги!
                //Создаем книгу, представляющую инфу о искомой книге! Из этой инфы - делаем объект книги и потом передаем этот объект книги в метод, выполняющий поиск книги -> получит книгу для поиск, вытащит из нее инфу, ищет и возвращается List - найденных Book'ов
                Book seaschedBook = new Book(NameBook_txbx.Text.Trim(), PublicherBook_txbx.Text.Trim(),
                    PublishedDateBook_txbx.Text, null, AuthorBook_txbx.Text.Trim());

                allBooks = accessToDataLibrury.GetSearchedBooks(seaschedBook);

                if (allBooks != null && allBooks.Count != 0)
                {
                    //ЗаполняемDGV
                    Books_dgv.DataSource = allBooks;
                }
            }

            if (Books_dgv.Columns != null && Books_dgv.Columns.Count != 0)
            {
                //Чтобы скрыть открытые поля из Books.cs
                Books_dgv.Columns["ItemFields"].Visible = false;
                Books_dgv.Columns["AuthorFields"].Visible = false;
                Books_dgv.Columns["BookFields"].Visible = false;
            }
        }

        private void addNewBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            #region Поэтому проще newBook - создать при вызове функции. Поэтому каждый раз будет новый List<Book> NewBook_ и не будет переполнения!

            //При этом он будет расти и книги будут сохранятсяя -> в итоге те книги которые, 
            //занесены в БД и хранятся в списке NewBook_ они будут излишне передаватся в DataSet, который будет проверять IsUniqueNewBook()
            //т.е. если мы эту книжку уже добавили, то добавив еще одну книгу у нас 2 книги: 
            //1. Первая проверяется на IsUniqueNewBook() - не проходит проверку, т.к мы ее только что добавили
            //2. а Вторая книга проходит! и в Бд идет вторая книга. и Получается если 100 записей такки, то 99 предыдущих будут проверятся и не проходить проверку. - лишняя работа, поэтмоу нужно чистить!

            #endregion

            //Надо получить Dialogresult
            NewBook newBook = new NewBook();
            //Надо получить Dialogresult && (не пустая ли коллекция?)
            if (newBook.ShowDialog() == DialogResult.OK && newBook.NewBook_.Count > 0)
            {
                //получить доступ к коллекции книг нашего newBook и передать в метод AddBook
                accessToDataLibrury.AddBook(newBook.NewBook_);
                RefreshBooks_btn_Click(null, null); //Всегда будет создан объект!
            }
            else
                MessageBox.Show("Нет новых книг для сохранения");
        }

        private void updateBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Создать с клиента новую книгу
            //конструктор по умолчанию, значения устанвоим через св-ва
            Book oldBook = new Book();
            oldBook.ID = new Guid(Books_dgv.CurrentRow.Cells["ID"].Value.ToString());
            oldBook.ISBN = Books_dgv.CurrentRow.Cells["ISBN"].Value.ToString();
            oldBook.Name = Books_dgv.CurrentRow.Cells["Name"].Value.ToString();
            oldBook.Publicher = Books_dgv.CurrentRow.Cells["Publicher"].Value.ToString(); //?? Publicher
            oldBook.PublichedDate = Books_dgv.CurrentRow.Cells["PublichedDate"].Value.ToString();
            oldBook.AuthorName = Books_dgv.CurrentRow.Cells["AuthorName"].Value.ToString();

            UpdateBook updateBook = new UpdateBook(oldBook);
            if (updateBook.ShowDialog() == DialogResult.OK)
            {
                List<Book> updatebleBooks = new List<Book>();
                updatebleBooks.Add(updateBook.NewBook);

                accessToDataLibrury.UpdateBooks(updatebleBooks);
            }
            RefreshBooks_btn_Click(null, null);
        }

        private void RefreshMag_btn_Click(object sender, EventArgs e)
        {
            if (accessToDataLibrury == null)
                return;

            List<Magazine> allMagazines;

            if ((sender == null && e == null) || //Если Все txbx  - пустые!
                (NameMag_txbx.Text.Trim() == string.Empty &&
                 PublicherMag_txbx.Text.Trim() == string.Empty &&
                 PublicherDateMag_txbx.Text.Trim() == string.Empty &&
                 IssueNumMag_txbx.Text.Trim() == string.Empty
                    )
                )
            {
                allMagazines = accessToDataLibrury.GetAllMagazines();
            }
            //Иначе произвести поиск. 1. создать объект прототипа искомой книги, 
            else
            {
                Magazine searchedMagazine = new Magazine(NameMag_txbx.Text.Trim(), PublicherMag_txbx.Text.Trim(),
                    PublicherDateMag_txbx.Text.Trim(), IssueNumMag_txbx.Text.Trim());

                //2 передать этот прототип в метод GetSearchedMagazines
                allMagazines = accessToDataLibrury.GetSearchedMagazines(searchedMagazine);
            }

            if (allMagazines != null)
                Magazines_dgv.DataSource = allMagazines;

            if (Magazines_dgv.Columns != null)
                //отключить вывод public  - полей для магазинов (Magazine.cs - два словаря!)
            {
                Magazines_dgv.Columns["ItemFields"].Visible = false;
                Magazines_dgv.Columns["MagazineFields"].Visible = false;
            }
        }

        private void RefreshArt_btn_Click(object sender, EventArgs e) //4.42
        {
            if (accessToDataLibrury == null)
                return;

            List<Article> allArticles = new List<Article>();

            if ((sender == null && e == null) ||
                NameArt_txbx.Text.Trim() == string.Empty &&
                PublicherArt_txbx.Text.Trim() == string.Empty &&
                PublicherDateArt_txbx.Text.Trim() == string.Empty &&
                AuthorArt_txbx.Text.Trim() == string.Empty)
            {
                allArticles = accessToDataLibrury.GetAllArticles(); //Если все пустое пришло, тогда метод GetAllArticles
            }
            else
            {
                Article searchedArticle = new Article(NameArt_txbx.Text.Trim(),
                    PublicherArt_txbx.Text.Trim(), PublicherDateArt_txbx.Text.Trim(),
                    null, null, AuthorArt_txbx.Text.Trim(), null);

                allArticles = accessToDataLibrury.GetSearchedArticles(searchedArticle); //Пришли данные, ищем!
            }

            if (allArticles != null)
                Article_dgv.DataSource = allArticles;

            if (Article_dgv.Columns != null && Article_dgv.Columns.Count > 0)
            {
                Article_dgv.Columns["ItemFields"].Visible = false;
                Article_dgv.Columns["MagazineFields"].Visible = false;
                Article_dgv.Columns["AuthorFields"].Visible = false;
                Article_dgv.Columns["ArticleFields"].Visible = false;
            }
        }

        private void addNewMagazineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Надо получить Dialogresult
            NewMagazine newMag = new NewMagazine();
            //Надо получить Dialogresult && (не пустая ли коллекция?)
            if (newMag.ShowDialog() == DialogResult.OK && newMag.NewMag.Count > 0)
            {
                //получить доступ к коллекции книг нашего newBook и передать в метод AddBook
                List<Magazine> notAddedMag = accessToDataLibrury.AddMagazine(newMag.NewMag);
                RefreshMag_btn_Click(null, null); //Всегда будет создан объект!

                if (notAddedMag.Count != 0)
                {
                    MessageBox.Show("Некоторые журналы не были сохранены В БД!");
                    NoAddedItems noAdIt = new NoAddedItems(notAddedMag);
                    noAdIt.ShowDialog();
                }
            }
            else
                MessageBox.Show("Нет новых журналов для сохранения");
        }

        private void addNewArticleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            newArticle = new NewArticle(accessToDataLibrury.GetAllMagazinesNames());

            newArticle.SelectMagName += new SelectItem(newArticle_SelectMagName);

            if (newArticle.ShowDialog() == DialogResult.OK && newArticle.NewArticles.Count > 0)
                accessToDataLibrury.AddArticle(newArticle.NewArticles);
            else
                MessageBox.Show("Нет статей для сохранения!");


            //Имея подписку на событие SelectMagName и пройдя if() - нужно убрать эту подписку!(отписатся)
            newArticle.SelectMagName -= new SelectItem(newArticle_SelectMagName);

            RefreshArt_btn_Click(null, null);
        }

        private void newArticle_SelectMagName(object sender, EventArgs e)
        {
            //Нужно получить список выпусков конкретного журнала.
            //Получить доступ к лист боксу NewArticle - MagazIssueNumber_ltbx, чтобы из Client Назначить DataSource (перечень выпусков конкретного журнала)
            newArticle.IssueNumLtbx.DataSource =
                accessToDataLibrury.GetCurrentMagazineAllIssueNumber(((ListBox) sender).SelectedValue.ToString());
        }

        private CopiesBook copBook;
        private void ShowAllCopiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //получить тек строку
            string bookName;
            List<Copie> bookCopies = accessToDataLibrury.GetBookCopies(new Guid(Books_dgv["ID", rowIndex.Y].FormattedValue.ToString()), out bookName);

            List<User> users = accessToDataLibrury.GetAllUsers();

            copBook = new CopiesBook(bookCopies, bookName, users); //выдаем копию
            copBook.BorrowingCopie += copBook_BorrowingCopie; //исп-ем событие вместо DialogResult!
            copBook.ShowDialog();
            copBook.BorrowingCopie -= copBook_BorrowingCopie; //исп-ем событие вместо DialogResult!
        }

        void copBook_BorrowingCopie(List<BorrowedCopie> BorCops)
        {
            accessToDataLibrury.BorrowingBook(BorCops);
        }

        Point rowIndex = new Point();
        private void Books_dgv_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            rowIndex.X = e.ColumnIndex;
            rowIndex.Y = e.RowIndex;
        }

        private UsersBorrowed borCop;
        private void showUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Открыть форму и подписатся на событие и потом динамиески менять строки для DGV
            borCop = new UsersBorrowed(accessToDataLibrury.GetAllUsers());
            borCop.SelectUserName += borCop_SelectUserName;
            if (borCop.ShowDialog() == DialogResult.OK)
                accessToDataLibrury.ReturnBooks(borCop.retunedItem);
            borCop.SelectUserName -= borCop_SelectUserName;
        }

        void borCop_SelectUserName(object sender, EventArgs e)
        {
            borCop.AllBorrowedCopieByUser_dgv.DataSource = accessToDataLibrury.GetAllBorrowedCopiesByUser(new Guid(((ListBox) sender).SelectedValue.ToString()));
        }

        private void addNewUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewUser newUser = new NewUser();
            if (newUser.ShowDialog() == DialogResult.OK)
            {
                if (accessToDataLibrury.AddUser(newUser.NewUser_))
                    MessageBox.Show("Новый пользователь создан.");
                else
                    MessageBox.Show("Новый пользователь НЕ создан.");
            }
        }

        private void Client_Load(object sender, EventArgs e)
        {
            Authorization authorization = new Authorization(accessToDataLibrury); //гл форма создастся в памяти, но мы не увидим ничего, появится Авториация
            if (authorization.ShowDialog() == DialogResult.OK)
            {
                //Вместо предварительной загрузки, просто имитируем нажатие кнопки, если все поля для поиска пустые!
                RefreshBooks_btn_Click(null, null);
                RefreshMag_btn_Click(null, null);
                RefreshArt_btn_Click(null, null);

                //Аутентификация - набор функций!
                if (!authorization.user.IsAdmin)
                    for (int i = 1; i < menuStrip1.Items.Count; i++)
                        menuStrip1.Items[i].Enabled = false;
                else
                    Books_dgv.ContextMenuStrip = Book_cntMstr;
            }
            else
                this.Close();
        }
    }
}