﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Common;

namespace Librury
{
    public partial class NewUser : Form
    {
        public User NewUser_ { get; private set; }
        public NewUser()
        {
            InitializeComponent();
        }

        private void newUser_btn_Click(object sender, EventArgs e)
        {
            if (UserName_txbx.Text.Trim() == string.Empty || Name_txbx.Text.Trim() == string.Empty ||
                Password_txbx.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Не все поля заполнены");
                return;
            }

            NewUser_ = new User(Guid.NewGuid(), Name_txbx.Text.Trim(), UserName_txbx.Text.Trim(),
                User.GetHashString(Password_txbx.Text.Trim()), isAdmin_chbx.Checked);

            DialogResult = DialogResult.OK;
        }
    }
}
