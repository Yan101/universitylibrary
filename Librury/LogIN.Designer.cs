﻿namespace Librury
{
    partial class LogIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.XML__rdbtn = new System.Windows.Forms.RadioButton();
            this.SQL_rdbtn = new System.Windows.Forms.RadioButton();
            this.SQL_gpbx = new System.Windows.Forms.GroupBox();
            this.UseWinAuth_chbx = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.InitialCatalog_txbx = new System.Windows.Forms.TextBox();
            this.ConnectionString_txbx = new System.Windows.Forms.TextBox();
            this.Password_txbx = new System.Windows.Forms.TextBox();
            this.Login_txbx = new System.Windows.Forms.TextBox();
            this.DataSource_txbx = new System.Windows.Forms.TextBox();
            this.XML_gpbx = new System.Windows.Forms.GroupBox();
            this.ChooseXmlFile_btn = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.XmlFile_txbx = new System.Windows.Forms.TextBox();
            this.Connect_btn = new System.Windows.Forms.Button();
            this.Cancel_btn = new System.Windows.Forms.Button();
            this.groupBox3.SuspendLayout();
            this.SQL_gpbx.SuspendLayout();
            this.XML_gpbx.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.XML__rdbtn);
            this.groupBox3.Controls.Add(this.SQL_rdbtn);
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(295, 47);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Choose Data Source";
            // 
            // XML__rdbtn
            // 
            this.XML__rdbtn.AutoSize = true;
            this.XML__rdbtn.Location = new System.Drawing.Point(183, 19);
            this.XML__rdbtn.Name = "XML__rdbtn";
            this.XML__rdbtn.Size = new System.Drawing.Size(95, 17);
            this.XML__rdbtn.TabIndex = 0;
            this.XML__rdbtn.TabStop = true;
            this.XML__rdbtn.Text = "Use XML Data";
            this.XML__rdbtn.UseVisualStyleBackColor = true;
            this.XML__rdbtn.CheckedChanged += new System.EventHandler(this.XML__rdbtn_CheckedChanged);
            // 
            // SQL_rdbtn
            // 
            this.SQL_rdbtn.AutoSize = true;
            this.SQL_rdbtn.CheckAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.SQL_rdbtn.Checked = true;
            this.SQL_rdbtn.Location = new System.Drawing.Point(89, 19);
            this.SQL_rdbtn.Name = "SQL_rdbtn";
            this.SQL_rdbtn.Size = new System.Drawing.Size(88, 17);
            this.SQL_rdbtn.TabIndex = 0;
            this.SQL_rdbtn.TabStop = true;
            this.SQL_rdbtn.Text = "Use Sql Data";
            this.SQL_rdbtn.UseVisualStyleBackColor = true;
            this.SQL_rdbtn.CheckedChanged += new System.EventHandler(this.SQL_rdbtn_CheckedChanged);
            // 
            // SQL_gpbx
            // 
            this.SQL_gpbx.Controls.Add(this.UseWinAuth_chbx);
            this.SQL_gpbx.Controls.Add(this.label6);
            this.SQL_gpbx.Controls.Add(this.label5);
            this.SQL_gpbx.Controls.Add(this.label4);
            this.SQL_gpbx.Controls.Add(this.label3);
            this.SQL_gpbx.Controls.Add(this.label2);
            this.SQL_gpbx.Controls.Add(this.label1);
            this.SQL_gpbx.Controls.Add(this.InitialCatalog_txbx);
            this.SQL_gpbx.Controls.Add(this.ConnectionString_txbx);
            this.SQL_gpbx.Controls.Add(this.Password_txbx);
            this.SQL_gpbx.Controls.Add(this.Login_txbx);
            this.SQL_gpbx.Controls.Add(this.DataSource_txbx);
            this.SQL_gpbx.Location = new System.Drawing.Point(12, 65);
            this.SQL_gpbx.Name = "SQL_gpbx";
            this.SQL_gpbx.Size = new System.Drawing.Size(295, 239);
            this.SQL_gpbx.TabIndex = 0;
            this.SQL_gpbx.TabStop = false;
            this.SQL_gpbx.Text = "Sql Connection";
            // 
            // UseWinAuth_chbx
            // 
            this.UseWinAuth_chbx.AutoSize = true;
            this.UseWinAuth_chbx.Location = new System.Drawing.Point(122, 71);
            this.UseWinAuth_chbx.Name = "UseWinAuth_chbx";
            this.UseWinAuth_chbx.Size = new System.Drawing.Size(15, 14);
            this.UseWinAuth_chbx.TabIndex = 2;
            this.UseWinAuth_chbx.UseVisualStyleBackColor = true;
            this.UseWinAuth_chbx.CheckedChanged += new System.EventHandler(this.ConnectionParam_txbx_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 143);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Connection String";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(41, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "User Password";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(61, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "User Login";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Use Win Authetication";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Initial Dialog";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Data Source";
            // 
            // InitialCatalog_txbx
            // 
            this.InitialCatalog_txbx.Location = new System.Drawing.Point(122, 45);
            this.InitialCatalog_txbx.Name = "InitialCatalog_txbx";
            this.InitialCatalog_txbx.Size = new System.Drawing.Size(157, 20);
            this.InitialCatalog_txbx.TabIndex = 0;
            this.InitialCatalog_txbx.Text = "UniversityLibrury";
            this.InitialCatalog_txbx.Leave += new System.EventHandler(this.ConnectionParam_txbx_Leave);
            // 
            // ConnectionString_txbx
            // 
            this.ConnectionString_txbx.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ConnectionString_txbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ConnectionString_txbx.Location = new System.Drawing.Point(9, 159);
            this.ConnectionString_txbx.Multiline = true;
            this.ConnectionString_txbx.Name = "ConnectionString_txbx";
            this.ConnectionString_txbx.Size = new System.Drawing.Size(273, 72);
            this.ConnectionString_txbx.TabIndex = 0;
            // 
            // Password_txbx
            // 
            this.Password_txbx.Location = new System.Drawing.Point(122, 117);
            this.Password_txbx.Name = "Password_txbx";
            this.Password_txbx.Size = new System.Drawing.Size(157, 20);
            this.Password_txbx.TabIndex = 0;
            this.Password_txbx.Leave += new System.EventHandler(this.ConnectionParam_txbx_Leave);
            // 
            // Login_txbx
            // 
            this.Login_txbx.Location = new System.Drawing.Point(122, 91);
            this.Login_txbx.Name = "Login_txbx";
            this.Login_txbx.Size = new System.Drawing.Size(157, 20);
            this.Login_txbx.TabIndex = 0;
            this.Login_txbx.Leave += new System.EventHandler(this.ConnectionParam_txbx_Leave);
            // 
            // DataSource_txbx
            // 
            this.DataSource_txbx.Location = new System.Drawing.Point(122, 19);
            this.DataSource_txbx.Name = "DataSource_txbx";
            this.DataSource_txbx.Size = new System.Drawing.Size(157, 20);
            this.DataSource_txbx.TabIndex = 0;
            this.DataSource_txbx.Text = "(local)";
            this.DataSource_txbx.TextChanged += new System.EventHandler(this.DataSource_txbx_TextChanged);
            this.DataSource_txbx.Leave += new System.EventHandler(this.ConnectionParam_txbx_Leave);
            // 
            // XML_gpbx
            // 
            this.XML_gpbx.Controls.Add(this.ChooseXmlFile_btn);
            this.XML_gpbx.Controls.Add(this.label7);
            this.XML_gpbx.Controls.Add(this.XmlFile_txbx);
            this.XML_gpbx.Location = new System.Drawing.Point(13, 310);
            this.XML_gpbx.Name = "XML_gpbx";
            this.XML_gpbx.Size = new System.Drawing.Size(294, 63);
            this.XML_gpbx.TabIndex = 1;
            this.XML_gpbx.TabStop = false;
            this.XML_gpbx.Text = "XML Connection";
            // 
            // ChooseXmlFile_btn
            // 
            this.ChooseXmlFile_btn.Location = new System.Drawing.Point(206, 34);
            this.ChooseXmlFile_btn.Name = "ChooseXmlFile_btn";
            this.ChooseXmlFile_btn.Size = new System.Drawing.Size(75, 23);
            this.ChooseXmlFile_btn.TabIndex = 2;
            this.ChooseXmlFile_btn.Text = "Look XML Data";
            this.ChooseXmlFile_btn.UseVisualStyleBackColor = true;
            this.ChooseXmlFile_btn.Click += new System.EventHandler(this.ChooseXmlFile_btn_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(131, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Choose XML Data Source";
            // 
            // XmlFile_txbx
            // 
            this.XmlFile_txbx.Location = new System.Drawing.Point(8, 35);
            this.XmlFile_txbx.Name = "XmlFile_txbx";
            this.XmlFile_txbx.Size = new System.Drawing.Size(192, 20);
            this.XmlFile_txbx.TabIndex = 0;
            // 
            // Connect_btn
            // 
            this.Connect_btn.Location = new System.Drawing.Point(12, 379);
            this.Connect_btn.Name = "Connect_btn";
            this.Connect_btn.Size = new System.Drawing.Size(214, 23);
            this.Connect_btn.TabIndex = 2;
            this.Connect_btn.Text = "Connect";
            this.Connect_btn.UseVisualStyleBackColor = true;
            this.Connect_btn.Click += new System.EventHandler(this.Connect_btn_Click);
            // 
            // Cancel_btn
            // 
            this.Cancel_btn.Location = new System.Drawing.Point(232, 379);
            this.Cancel_btn.Name = "Cancel_btn";
            this.Cancel_btn.Size = new System.Drawing.Size(75, 23);
            this.Cancel_btn.TabIndex = 2;
            this.Cancel_btn.Text = "Cancel";
            this.Cancel_btn.UseVisualStyleBackColor = true;
            this.Cancel_btn.Click += new System.EventHandler(this.Cancel_btn_Click);
            // 
            // LogIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 411);
            this.Controls.Add(this.Cancel_btn);
            this.Controls.Add(this.Connect_btn);
            this.Controls.Add(this.XML_gpbx);
            this.Controls.Add(this.SQL_gpbx);
            this.Controls.Add(this.groupBox3);
            this.Name = "LogIn";
            this.Text = "LogIn";
            this.Load += new System.EventHandler(this.LogIn_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.SQL_gpbx.ResumeLayout(false);
            this.SQL_gpbx.PerformLayout();
            this.XML_gpbx.ResumeLayout(false);
            this.XML_gpbx.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton XML__rdbtn;
        private System.Windows.Forms.RadioButton SQL_rdbtn;
        private System.Windows.Forms.GroupBox SQL_gpbx;
        private System.Windows.Forms.CheckBox UseWinAuth_chbx;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox InitialCatalog_txbx;
        private System.Windows.Forms.TextBox ConnectionString_txbx;
        private System.Windows.Forms.TextBox Password_txbx;
        private System.Windows.Forms.TextBox Login_txbx;
        private System.Windows.Forms.TextBox DataSource_txbx;
        private System.Windows.Forms.GroupBox XML_gpbx;
        private System.Windows.Forms.Button ChooseXmlFile_btn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox XmlFile_txbx;
        private System.Windows.Forms.Button Connect_btn;
        private System.Windows.Forms.Button Cancel_btn;
    }
}

