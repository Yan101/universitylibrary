﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Common;

namespace Librury
{
    public partial class Authorization : Form
    {
        private AccessToDataLibrury.AccessToDataLibrury accessToDataLibrury;
        public User user;
        public Authorization(AccessToDataLibrury.AccessToDataLibrury accessToDataLibrury)
        {
            InitializeComponent();
            this.accessToDataLibrury = accessToDataLibrury;
        }

        private void Login_btn_Click(object sender, EventArgs e)
        {
            user = accessToDataLibrury.CheckUser(UserName_txbx.Text.Trim(), User.GetHashString(Password_txbx.Text.Trim()));

            if (user != null)
                DialogResult = DialogResult.OK;
            else
            {
                MessageBox.Show("Такая пара Логин / Пароль не значатся в БД.");
                return;
            }
        }
    }
}
