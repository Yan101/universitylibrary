﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AccessToDataLibrury;
using Common;
using AccessToDataLibrury = AccessToDataLibrury.AccessToDataLibrury;

namespace Librury
{
    public delegate void BorCopie(List<BorrowedCopie> BorCops);
    public partial class CopiesBook : Form
    {
        //Выдача книги! формируем обьект и список
        private string bookName;
        public event BorCopie BorrowingCopie;

        public CopiesBook(List<Copie> copies, string bookName, List<User> users )
        {
            InitializeComponent();

            this.bookName = bookName;

            BookCopies_dgv.DataSource = copies;
            BookName_lbl.Text += bookName;

            users_ltbx.DataSource = users;
            users_ltbx.DisplayMember = "Name";
            users_ltbx.ValueMember = "ID";
        }

        private void BookCopies_dgv_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (e.RowIndex == -1) return;

            if (Boolean.Parse(BookCopies_dgv["IsBorrowed", e.RowIndex].FormattedValue.ToString()))
                BookCopies_dgv.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Red;
        }

        private void BookCopies_dgv_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            bookCopieID_txbx.Text = BookCopies_dgv["ID", e.RowIndex].FormattedValue.ToString();

            borrowBook_btn.Enabled = true;

            if (Boolean.Parse(BookCopies_dgv["IsBorrowed", e.RowIndex].FormattedValue.ToString()))
                borrowBook_btn.Enabled = false;
        }

        private void borrowBook_btn_Click(object sender, EventArgs e)
        {
            BorrowedCopie borCop = new BorrowedCopie(new Guid(bookCopieID_txbx.Text), new Guid(users_ltbx.SelectedValue.ToString()), DateTime.Now, bookName);

            List<BorrowedCopie> BorCops = new List<BorrowedCopie>();
            BorCops.Add(borCop);

            //через события и делегаты
            if (BorrowingCopie != null)
                BorrowingCopie(BorCops);
        }

     
    }
}
