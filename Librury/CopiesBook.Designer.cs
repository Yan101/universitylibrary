﻿namespace Librury
{
    partial class CopiesBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BookName_lbl = new System.Windows.Forms.Label();
            this.BookCopies_dgv = new System.Windows.Forms.DataGridView();
            this.bookCopieID_txbx = new System.Windows.Forms.TextBox();
            this.users_ltbx = new System.Windows.Forms.ListBox();
            this.borrowBook_btn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.BookCopies_dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // BookName_lbl
            // 
            this.BookName_lbl.AutoSize = true;
            this.BookName_lbl.Location = new System.Drawing.Point(12, 9);
            this.BookName_lbl.Name = "BookName_lbl";
            this.BookName_lbl.Size = new System.Drawing.Size(89, 13);
            this.BookName_lbl.TabIndex = 0;
            this.BookName_lbl.Text = "Название книги";
            // 
            // BookCopies_dgv
            // 
            this.BookCopies_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.BookCopies_dgv.Location = new System.Drawing.Point(13, 30);
            this.BookCopies_dgv.Name = "BookCopies_dgv";
            this.BookCopies_dgv.Size = new System.Drawing.Size(700, 318);
            this.BookCopies_dgv.TabIndex = 1;
            this.BookCopies_dgv.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.BookCopies_dgv_CellEnter);
            this.BookCopies_dgv.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.BookCopies_dgv_RowPrePaint);
            // 
            // bookCopieID_txbx
            // 
            this.bookCopieID_txbx.Location = new System.Drawing.Point(13, 355);
            this.bookCopieID_txbx.Name = "bookCopieID_txbx";
            this.bookCopieID_txbx.ReadOnly = true;
            this.bookCopieID_txbx.Size = new System.Drawing.Size(298, 20);
            this.bookCopieID_txbx.TabIndex = 2;
            // 
            // users_ltbx
            // 
            this.users_ltbx.FormattingEnabled = true;
            this.users_ltbx.Location = new System.Drawing.Point(317, 355);
            this.users_ltbx.Name = "users_ltbx";
            this.users_ltbx.Size = new System.Drawing.Size(261, 82);
            this.users_ltbx.TabIndex = 3;
            // 
            // borrowBook_btn
            // 
            this.borrowBook_btn.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.borrowBook_btn.ForeColor = System.Drawing.Color.DarkGreen;
            this.borrowBook_btn.Location = new System.Drawing.Point(584, 355);
            this.borrowBook_btn.Name = "borrowBook_btn";
            this.borrowBook_btn.Size = new System.Drawing.Size(129, 82);
            this.borrowBook_btn.TabIndex = 4;
            this.borrowBook_btn.Text = "Выдать книгу";
            this.borrowBook_btn.UseVisualStyleBackColor = true;
            this.borrowBook_btn.Click += new System.EventHandler(this.borrowBook_btn_Click);
            // 
            // CopiesBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(725, 447);
            this.Controls.Add(this.borrowBook_btn);
            this.Controls.Add(this.users_ltbx);
            this.Controls.Add(this.bookCopieID_txbx);
            this.Controls.Add(this.BookCopies_dgv);
            this.Controls.Add(this.BookName_lbl);
            this.Name = "CopiesBook";
            this.Text = "CopiesBook";
            ((System.ComponentModel.ISupportInitialize)(this.BookCopies_dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label BookName_lbl;
        private System.Windows.Forms.DataGridView BookCopies_dgv;
        private System.Windows.Forms.TextBox bookCopieID_txbx;
        private System.Windows.Forms.ListBox users_ltbx;
        private System.Windows.Forms.Button borrowBook_btn;
    }
}