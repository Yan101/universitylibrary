﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Common;

namespace Librury
{
    public partial class NewMagazine : Form
    {
        public List<Magazine> NewMag { get; private set; }
        public NewMagazine()
        {
            InitializeComponent();
            NewMag = new List<Magazine>();
        }

        private void Add_magazine_btn_Click(object sender, EventArgs e)
        {
            string name = Name_txbx.Text.Trim();
            string publicher = Publicher_txbx.Text.Trim();
            string publicherDate = PublicherDate_txbx.Text.Trim();
            string issueNumber = IssueNum_txbx.Text.Trim();

            if (name != string.Empty && publicher != string.Empty && publicherDate != string.Empty &&
                issueNumber != string.Empty)
            {
                //Проверка текущей коллекции на повторение записей!!!
                foreach (Magazine m in NewMag)
                    if (m.Name == name && m.Publicher == publicher && m.PublichedDate == publicherDate &&
                        m.IssueNumber == issueNumber)
                    {
                        MessageBox.Show("Такой журнал вы уже добавляли!");
                        return;
                    }

                NewMag.Add(new Magazine(name, publicher, publicherDate, issueNumber));
                countMag_lbl.Text = "Новых журналов: " + NewMag.Count;
                return;
            }
            else
                MessageBox.Show("Не все поля заполнены");
        }

        private void save_magazine_btn_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
