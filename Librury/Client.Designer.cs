﻿namespace Librury
{
    partial class Client
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Items_tbcl = new System.Windows.Forms.TabPage();
            this.PublishedDateBook_txbx = new System.Windows.Forms.TextBox();
            this.RefreshBooks_btn = new System.Windows.Forms.Button();
            this.AuthorBook_txbx = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.PublicherBook_txbx = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.NameBook_txbx = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Books_dgv = new System.Windows.Forms.DataGridView();
            this.Book_cntMstr = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ShowAllCopiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.SearchArt_btn = new System.Windows.Forms.Button();
            this.PublicherDateArt_txbx = new System.Windows.Forms.TextBox();
            this.PublicherDateMag_txbx = new System.Windows.Forms.TextBox();
            this.RefreshMag_btn = new System.Windows.Forms.Button();
            this.AuthorArt_txbx = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.PublicherArt_txbx = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.NameArt_txbx = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.IssueNumMag_txbx = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.PublicherMag_txbx = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.NameMag_txbx = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Article_dgv = new System.Windows.Forms.DataGridView();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.Magazines_dgv = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewBookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateBookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.magazineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewMagazineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.articleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewArticleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1.SuspendLayout();
            this.Items_tbcl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Books_dgv)).BeginInit();
            this.Book_cntMstr.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Article_dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Magazines_dgv)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Items_tbcl);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 27);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(862, 489);
            this.tabControl1.TabIndex = 2;
            // 
            // Items_tbcl
            // 
            this.Items_tbcl.Controls.Add(this.PublishedDateBook_txbx);
            this.Items_tbcl.Controls.Add(this.RefreshBooks_btn);
            this.Items_tbcl.Controls.Add(this.AuthorBook_txbx);
            this.Items_tbcl.Controls.Add(this.label4);
            this.Items_tbcl.Controls.Add(this.label3);
            this.Items_tbcl.Controls.Add(this.PublicherBook_txbx);
            this.Items_tbcl.Controls.Add(this.label2);
            this.Items_tbcl.Controls.Add(this.NameBook_txbx);
            this.Items_tbcl.Controls.Add(this.label1);
            this.Items_tbcl.Controls.Add(this.Books_dgv);
            this.Items_tbcl.Location = new System.Drawing.Point(4, 22);
            this.Items_tbcl.Name = "Items_tbcl";
            this.Items_tbcl.Padding = new System.Windows.Forms.Padding(3);
            this.Items_tbcl.Size = new System.Drawing.Size(854, 463);
            this.Items_tbcl.TabIndex = 0;
            this.Items_tbcl.Text = "Books";
            this.Items_tbcl.UseVisualStyleBackColor = true;
            // 
            // PublishedDateBook_txbx
            // 
            this.PublishedDateBook_txbx.Location = new System.Drawing.Point(385, 19);
            this.PublishedDateBook_txbx.Name = "PublishedDateBook_txbx";
            this.PublishedDateBook_txbx.Size = new System.Drawing.Size(134, 20);
            this.PublishedDateBook_txbx.TabIndex = 2;
            // 
            // RefreshBooks_btn
            // 
            this.RefreshBooks_btn.Location = new System.Drawing.Point(698, 16);
            this.RefreshBooks_btn.Name = "RefreshBooks_btn";
            this.RefreshBooks_btn.Size = new System.Drawing.Size(150, 25);
            this.RefreshBooks_btn.TabIndex = 4;
            this.RefreshBooks_btn.Text = "Refresh";
            this.RefreshBooks_btn.UseVisualStyleBackColor = true;
            this.RefreshBooks_btn.Click += new System.EventHandler(this.RefreshBooks_btn_Click);
            // 
            // AuthorBook_txbx
            // 
            this.AuthorBook_txbx.Location = new System.Drawing.Point(525, 19);
            this.AuthorBook_txbx.Name = "AuthorBook_txbx";
            this.AuthorBook_txbx.Size = new System.Drawing.Size(167, 20);
            this.AuthorBook_txbx.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(522, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Author";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(382, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Publicher Date";
            // 
            // PublicherBook_txbx
            // 
            this.PublicherBook_txbx.Location = new System.Drawing.Point(165, 19);
            this.PublicherBook_txbx.Name = "PublicherBook_txbx";
            this.PublicherBook_txbx.Size = new System.Drawing.Size(214, 20);
            this.PublicherBook_txbx.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(162, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Publicher";
            // 
            // NameBook_txbx
            // 
            this.NameBook_txbx.Location = new System.Drawing.Point(9, 19);
            this.NameBook_txbx.Name = "NameBook_txbx";
            this.NameBook_txbx.Size = new System.Drawing.Size(150, 20);
            this.NameBook_txbx.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Name";
            // 
            // Books_dgv
            // 
            this.Books_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Books_dgv.Location = new System.Drawing.Point(9, 45);
            this.Books_dgv.Name = "Books_dgv";
            this.Books_dgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.Books_dgv.Size = new System.Drawing.Size(839, 382);
            this.Books_dgv.TabIndex = 0;
            this.Books_dgv.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.Books_dgv_CellMouseEnter);
            // 
            // Book_cntMstr
            // 
            this.Book_cntMstr.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ShowAllCopiesToolStripMenuItem});
            this.Book_cntMstr.Name = "Book_cntMstr";
            this.Book_cntMstr.Size = new System.Drawing.Size(182, 28);
            // 
            // ShowAllCopiesToolStripMenuItem
            // 
            this.ShowAllCopiesToolStripMenuItem.Name = "ShowAllCopiesToolStripMenuItem";
            this.ShowAllCopiesToolStripMenuItem.Size = new System.Drawing.Size(181, 24);
            this.ShowAllCopiesToolStripMenuItem.Text = "Show all copies";
            this.ShowAllCopiesToolStripMenuItem.Click += new System.EventHandler(this.ShowAllCopiesToolStripMenuItem_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.SearchArt_btn);
            this.tabPage2.Controls.Add(this.PublicherDateArt_txbx);
            this.tabPage2.Controls.Add(this.PublicherDateMag_txbx);
            this.tabPage2.Controls.Add(this.RefreshMag_btn);
            this.tabPage2.Controls.Add(this.AuthorArt_txbx);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.PublicherArt_txbx);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.NameArt_txbx);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.IssueNumMag_txbx);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.PublicherMag_txbx);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.NameMag_txbx);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.Article_dgv);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.Magazines_dgv);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(854, 463);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Magazines/Articles";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // SearchArt_btn
            // 
            this.SearchArt_btn.Location = new System.Drawing.Point(723, 243);
            this.SearchArt_btn.Name = "SearchArt_btn";
            this.SearchArt_btn.Size = new System.Drawing.Size(125, 23);
            this.SearchArt_btn.TabIndex = 32;
            this.SearchArt_btn.Text = "Search";
            this.SearchArt_btn.UseVisualStyleBackColor = true;
            this.SearchArt_btn.Click += new System.EventHandler(this.RefreshArt_btn_Click);
            // 
            // PublicherDateArt_txbx
            // 
            this.PublicherDateArt_txbx.Location = new System.Drawing.Point(432, 245);
            this.PublicherDateArt_txbx.Name = "PublicherDateArt_txbx";
            this.PublicherDateArt_txbx.Size = new System.Drawing.Size(140, 20);
            this.PublicherDateArt_txbx.TabIndex = 31;
            // 
            // PublicherDateMag_txbx
            // 
            this.PublicherDateMag_txbx.Location = new System.Drawing.Point(432, 35);
            this.PublicherDateMag_txbx.Name = "PublicherDateMag_txbx";
            this.PublicherDateMag_txbx.Size = new System.Drawing.Size(140, 20);
            this.PublicherDateMag_txbx.TabIndex = 30;
            // 
            // RefreshMag_btn
            // 
            this.RefreshMag_btn.Location = new System.Drawing.Point(723, 31);
            this.RefreshMag_btn.Name = "RefreshMag_btn";
            this.RefreshMag_btn.Size = new System.Drawing.Size(125, 23);
            this.RefreshMag_btn.TabIndex = 29;
            this.RefreshMag_btn.Text = "Search(Ref Mag)";
            this.RefreshMag_btn.UseVisualStyleBackColor = true;
            this.RefreshMag_btn.Click += new System.EventHandler(this.RefreshMag_btn_Click);
            // 
            // AuthorArt_txbx
            // 
            this.AuthorArt_txbx.Location = new System.Drawing.Point(578, 245);
            this.AuthorArt_txbx.Name = "AuthorArt_txbx";
            this.AuthorArt_txbx.Size = new System.Drawing.Size(139, 20);
            this.AuthorArt_txbx.TabIndex = 25;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(575, 229);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Author";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(429, 229);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Publicher Date";
            // 
            // PublicherArt_txbx
            // 
            this.PublicherArt_txbx.Location = new System.Drawing.Point(236, 245);
            this.PublicherArt_txbx.Name = "PublicherArt_txbx";
            this.PublicherArt_txbx.Size = new System.Drawing.Size(190, 20);
            this.PublicherArt_txbx.TabIndex = 26;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(233, 229);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "Publicher";
            // 
            // NameArt_txbx
            // 
            this.NameArt_txbx.Location = new System.Drawing.Point(12, 245);
            this.NameArt_txbx.Name = "NameArt_txbx";
            this.NameArt_txbx.Size = new System.Drawing.Size(218, 20);
            this.NameArt_txbx.TabIndex = 27;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 229);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "Name";
            // 
            // IssueNumMag_txbx
            // 
            this.IssueNumMag_txbx.Location = new System.Drawing.Point(578, 34);
            this.IssueNumMag_txbx.Name = "IssueNumMag_txbx";
            this.IssueNumMag_txbx.Size = new System.Drawing.Size(139, 20);
            this.IssueNumMag_txbx.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(575, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Issue Num";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(429, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Publicher Date";
            // 
            // PublicherMag_txbx
            // 
            this.PublicherMag_txbx.Location = new System.Drawing.Point(236, 35);
            this.PublicherMag_txbx.Name = "PublicherMag_txbx";
            this.PublicherMag_txbx.Size = new System.Drawing.Size(190, 20);
            this.PublicherMag_txbx.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(233, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Publicher";
            // 
            // NameMag_txbx
            // 
            this.NameMag_txbx.Location = new System.Drawing.Point(12, 35);
            this.NameMag_txbx.Name = "NameMag_txbx";
            this.NameMag_txbx.Size = new System.Drawing.Size(218, 20);
            this.NameMag_txbx.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Name";
            // 
            // Article_dgv
            // 
            this.Article_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Article_dgv.Location = new System.Drawing.Point(12, 271);
            this.Article_dgv.Name = "Article_dgv";
            this.Article_dgv.Size = new System.Drawing.Size(836, 186);
            this.Article_dgv.TabIndex = 12;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(412, 214);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 16);
            this.label14.TabIndex = 7;
            this.label14.Text = "Articles";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(401, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 16);
            this.label13.TabIndex = 7;
            this.label13.Text = "Magazines";
            // 
            // Magazines_dgv
            // 
            this.Magazines_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Magazines_dgv.Location = new System.Drawing.Point(12, 61);
            this.Magazines_dgv.Name = "Magazines_dgv";
            this.Magazines_dgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.Magazines_dgv.Size = new System.Drawing.Size(836, 150);
            this.Magazines_dgv.TabIndex = 3;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.bookToolStripMenuItem,
            this.magazineToolStripMenuItem,
            this.articleToolStripMenuItem,
            this.usersToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(886, 28);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // bookToolStripMenuItem
            // 
            this.bookToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewBookToolStripMenuItem,
            this.updateBookToolStripMenuItem});
            this.bookToolStripMenuItem.Name = "bookToolStripMenuItem";
            this.bookToolStripMenuItem.Size = new System.Drawing.Size(55, 24);
            this.bookToolStripMenuItem.Text = "Book";
            // 
            // addNewBookToolStripMenuItem
            // 
            this.addNewBookToolStripMenuItem.Name = "addNewBookToolStripMenuItem";
            this.addNewBookToolStripMenuItem.Size = new System.Drawing.Size(175, 24);
            this.addNewBookToolStripMenuItem.Text = "Add new Book";
            this.addNewBookToolStripMenuItem.Click += new System.EventHandler(this.addNewBookToolStripMenuItem_Click);
            // 
            // updateBookToolStripMenuItem
            // 
            this.updateBookToolStripMenuItem.Name = "updateBookToolStripMenuItem";
            this.updateBookToolStripMenuItem.Size = new System.Drawing.Size(175, 24);
            this.updateBookToolStripMenuItem.Text = "Update Book";
            this.updateBookToolStripMenuItem.Click += new System.EventHandler(this.updateBookToolStripMenuItem_Click);
            // 
            // magazineToolStripMenuItem
            // 
            this.magazineToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewMagazineToolStripMenuItem});
            this.magazineToolStripMenuItem.Name = "magazineToolStripMenuItem";
            this.magazineToolStripMenuItem.Size = new System.Drawing.Size(86, 24);
            this.magazineToolStripMenuItem.Text = "Magazine";
            // 
            // addNewMagazineToolStripMenuItem
            // 
            this.addNewMagazineToolStripMenuItem.Name = "addNewMagazineToolStripMenuItem";
            this.addNewMagazineToolStripMenuItem.Size = new System.Drawing.Size(209, 24);
            this.addNewMagazineToolStripMenuItem.Text = "Add New Magazine";
            this.addNewMagazineToolStripMenuItem.Click += new System.EventHandler(this.addNewMagazineToolStripMenuItem_Click);
            // 
            // articleToolStripMenuItem
            // 
            this.articleToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewArticleToolStripMenuItem});
            this.articleToolStripMenuItem.Name = "articleToolStripMenuItem";
            this.articleToolStripMenuItem.Size = new System.Drawing.Size(64, 24);
            this.articleToolStripMenuItem.Text = "Article";
            // 
            // addNewArticleToolStripMenuItem
            // 
            this.addNewArticleToolStripMenuItem.Name = "addNewArticleToolStripMenuItem";
            this.addNewArticleToolStripMenuItem.Size = new System.Drawing.Size(184, 24);
            this.addNewArticleToolStripMenuItem.Text = "Add new Article";
            this.addNewArticleToolStripMenuItem.Click += new System.EventHandler(this.addNewArticleToolStripMenuItem_Click);
            // 
            // usersToolStripMenuItem
            // 
            this.usersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showUserToolStripMenuItem,
            this.addNewUserToolStripMenuItem});
            this.usersToolStripMenuItem.Name = "usersToolStripMenuItem";
            this.usersToolStripMenuItem.Size = new System.Drawing.Size(56, 24);
            this.usersToolStripMenuItem.Text = "Users";
            // 
            // showUserToolStripMenuItem
            // 
            this.showUserToolStripMenuItem.Name = "showUserToolStripMenuItem";
            this.showUserToolStripMenuItem.Size = new System.Drawing.Size(168, 24);
            this.showUserToolStripMenuItem.Text = "Show user";
            this.showUserToolStripMenuItem.Click += new System.EventHandler(this.showUserToolStripMenuItem_Click);
            // 
            // addNewUserToolStripMenuItem
            // 
            this.addNewUserToolStripMenuItem.Name = "addNewUserToolStripMenuItem";
            this.addNewUserToolStripMenuItem.Size = new System.Drawing.Size(168, 24);
            this.addNewUserToolStripMenuItem.Text = "Add new user";
            this.addNewUserToolStripMenuItem.Click += new System.EventHandler(this.addNewUserToolStripMenuItem_Click);
            // 
            // Client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(886, 521);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Client";
            this.Text = "Client";
            this.Load += new System.EventHandler(this.Client_Load);
            this.tabControl1.ResumeLayout(false);
            this.Items_tbcl.ResumeLayout(false);
            this.Items_tbcl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Books_dgv)).EndInit();
            this.Book_cntMstr.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Article_dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Magazines_dgv)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Items_tbcl;
        private System.Windows.Forms.TextBox AuthorBook_txbx;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox PublicherBook_txbx;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox NameBook_txbx;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView Books_dgv;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView Article_dgv;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridView Magazines_dgv;
        private System.Windows.Forms.Button RefreshBooks_btn;
        private System.Windows.Forms.TextBox AuthorArt_txbx;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox PublicherArt_txbx;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox NameArt_txbx;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox IssueNumMag_txbx;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox PublicherMag_txbx;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox NameMag_txbx;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox PublishedDateBook_txbx;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bookToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNewBookToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateBookToolStripMenuItem;
        private System.Windows.Forms.Button RefreshMag_btn;
        private System.Windows.Forms.TextBox PublicherDateMag_txbx;
        private System.Windows.Forms.Button SearchArt_btn;
        private System.Windows.Forms.TextBox PublicherDateArt_txbx;
        private System.Windows.Forms.ToolStripMenuItem magazineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNewMagazineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem articleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNewArticleToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip Book_cntMstr;
        private System.Windows.Forms.ToolStripMenuItem ShowAllCopiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNewUserToolStripMenuItem;

    }
}