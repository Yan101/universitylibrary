﻿namespace Librury
{
    partial class NewBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Name_txbx = new System.Windows.Forms.TextBox();
            this.Publicher_txbx = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.PublicherDate_txbx = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ISBN_txbx = new System.Windows.Forms.TextBox();
            this.ISBN = new System.Windows.Forms.Label();
            this.AuthorName_txbx = new System.Windows.Forms.TextBox();
            this.AuthorName = new System.Windows.Forms.Label();
            this.AddBook_btn = new System.Windows.Forms.Button();
            this.SaveBooks_btn = new System.Windows.Forms.Button();
            this.bookCount_lbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Title";
            // 
            // Name_txbx
            // 
            this.Name_txbx.Location = new System.Drawing.Point(92, 12);
            this.Name_txbx.Name = "Name_txbx";
            this.Name_txbx.Size = new System.Drawing.Size(165, 20);
            this.Name_txbx.TabIndex = 1;
            // 
            // Publicher_txbx
            // 
            this.Publicher_txbx.Location = new System.Drawing.Point(92, 38);
            this.Publicher_txbx.Name = "Publicher_txbx";
            this.Publicher_txbx.Size = new System.Drawing.Size(165, 20);
            this.Publicher_txbx.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Publicher";
            // 
            // PublicherDate_txbx
            // 
            this.PublicherDate_txbx.Location = new System.Drawing.Point(92, 64);
            this.PublicherDate_txbx.Name = "PublicherDate_txbx";
            this.PublicherDate_txbx.Size = new System.Drawing.Size(165, 20);
            this.PublicherDate_txbx.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Publicher Date";
            // 
            // ISBN_txbx
            // 
            this.ISBN_txbx.Location = new System.Drawing.Point(92, 90);
            this.ISBN_txbx.Name = "ISBN_txbx";
            this.ISBN_txbx.Size = new System.Drawing.Size(165, 20);
            this.ISBN_txbx.TabIndex = 7;
            // 
            // ISBN
            // 
            this.ISBN.AutoSize = true;
            this.ISBN.Location = new System.Drawing.Point(12, 93);
            this.ISBN.Name = "ISBN";
            this.ISBN.Size = new System.Drawing.Size(32, 13);
            this.ISBN.TabIndex = 6;
            this.ISBN.Text = "ISBN";
            // 
            // AuthorName_txbx
            // 
            this.AuthorName_txbx.Location = new System.Drawing.Point(92, 116);
            this.AuthorName_txbx.Name = "AuthorName_txbx";
            this.AuthorName_txbx.Size = new System.Drawing.Size(165, 20);
            this.AuthorName_txbx.TabIndex = 9;
            // 
            // AuthorName
            // 
            this.AuthorName.AutoSize = true;
            this.AuthorName.Location = new System.Drawing.Point(12, 119);
            this.AuthorName.Name = "AuthorName";
            this.AuthorName.Size = new System.Drawing.Size(69, 13);
            this.AuthorName.TabIndex = 8;
            this.AuthorName.Text = "Author Name";
            // 
            // AddBook_btn
            // 
            this.AddBook_btn.Location = new System.Drawing.Point(92, 143);
            this.AddBook_btn.Name = "AddBook_btn";
            this.AddBook_btn.Size = new System.Drawing.Size(165, 23);
            this.AddBook_btn.TabIndex = 10;
            this.AddBook_btn.Text = "Add Book";
            this.AddBook_btn.UseVisualStyleBackColor = true;
            this.AddBook_btn.Click += new System.EventHandler(this.AddBook_btn_Click);
            // 
            // SaveBooks_btn
            // 
            this.SaveBooks_btn.Location = new System.Drawing.Point(92, 172);
            this.SaveBooks_btn.Name = "SaveBooks_btn";
            this.SaveBooks_btn.Size = new System.Drawing.Size(165, 23);
            this.SaveBooks_btn.TabIndex = 10;
            this.SaveBooks_btn.Text = "Save all new Books";
            this.SaveBooks_btn.UseVisualStyleBackColor = true;
            this.SaveBooks_btn.Click += new System.EventHandler(this.SaveBooks_btn_Click);
            // 
            // bookCount_lbl
            // 
            this.bookCount_lbl.AutoSize = true;
            this.bookCount_lbl.Location = new System.Drawing.Point(12, 177);
            this.bookCount_lbl.Name = "bookCount_lbl";
            this.bookCount_lbl.Size = new System.Drawing.Size(69, 13);
            this.bookCount_lbl.TabIndex = 11;
            this.bookCount_lbl.Text = "Новых книг:";
            // 
            // NewBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(269, 200);
            this.Controls.Add(this.bookCount_lbl);
            this.Controls.Add(this.SaveBooks_btn);
            this.Controls.Add(this.AddBook_btn);
            this.Controls.Add(this.AuthorName_txbx);
            this.Controls.Add(this.AuthorName);
            this.Controls.Add(this.ISBN_txbx);
            this.Controls.Add(this.ISBN);
            this.Controls.Add(this.PublicherDate_txbx);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Publicher_txbx);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Name_txbx);
            this.Controls.Add(this.label1);
            this.Name = "NewBook";
            this.Text = "NewBook";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Name_txbx;
        private System.Windows.Forms.TextBox Publicher_txbx;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox PublicherDate_txbx;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox ISBN_txbx;
        private System.Windows.Forms.Label ISBN;
        private System.Windows.Forms.TextBox AuthorName_txbx;
        private System.Windows.Forms.Label AuthorName;
        private System.Windows.Forms.Button AddBook_btn;
        private System.Windows.Forms.Button SaveBooks_btn;
        private System.Windows.Forms.Label bookCount_lbl;
    }
}