﻿namespace Librury
{
    partial class NoAddedItems
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.notAddedItems_dgv = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.notAddedItems_dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // notAddedItems_dgv
            // 
            this.notAddedItems_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.notAddedItems_dgv.Location = new System.Drawing.Point(13, 13);
            this.notAddedItems_dgv.Name = "notAddedItems_dgv";
            this.notAddedItems_dgv.Size = new System.Drawing.Size(836, 236);
            this.notAddedItems_dgv.TabIndex = 0;
            // 
            // NoAddedItems
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(861, 261);
            this.Controls.Add(this.notAddedItems_dgv);
            this.Name = "NoAddedItems";
            this.Text = "NoAddedItems";
            ((System.ComponentModel.ISupportInitialize)(this.notAddedItems_dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView notAddedItems_dgv;
    }
}