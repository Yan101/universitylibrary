﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using Common;

namespace Librury
{
    public partial class NewBook : Form
    {
        public List<Book> NewBook_ { get; private set; }

        public NewBook()
        {
            InitializeComponent();
            NewBook_ = new List<Book>();
        }
        //каждый раз при нажатии будет происходить дополнение новыми книгами списка новыми NewBook_. 
          private void AddBook_btn_Click(object sender, EventArgs e) 
        {
            string name = Name_txbx.Text.Trim();
            string Publicher = Publicher_txbx.Text.Trim();
            string PublicherDate = PublicherDate_txbx.Text.Trim();
            string iSBN = ISBN_txbx.Text.Trim();
            string authorName = AuthorName_txbx.Text.Trim();
            
            //Все ли поля заполнены
            if  (name != string.Empty &&
                 Publicher != string.Empty &&
                 PublicherDate != string.Empty &&
                 iSBN != string.Empty &&
                 authorName != string.Empty) //Если не пусто, то можно устанавливать св-ва книги
            {
                NewBook_.Add(new Book(name, Publicher, PublicherDate, iSBN, authorName));
                bookCount_lbl.Text = "Новых книг: " + NewBook_.Count;
                return;
            }

            MessageBox.Show("Не все поля указаны");
            return;
        }

        private void SaveBooks_btn_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
