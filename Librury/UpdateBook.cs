﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Common;//Book

namespace Librury
{
    public partial class UpdateBook : Form
    {
        Book oldBook; //та инфа которая есть в БД
        public Book NewBook { get; private set; }

        public UpdateBook(Book oldBook)
        {
            InitializeComponent();
            this.oldBook = oldBook;
        }

        private void UpdateBook_btn_Click(object sender, EventArgs e)
        {
            if (
                Name_txbx.Text.Trim() == "" ||
                Publicher_txbx.Text.Trim() == "" ||
                PublicherDate_txbx.Text.Trim() == "" ||
                ISBN_txbx.Text.Trim() == "" ||
                AuthorName_txbx.Text.Trim() == ""
                )
            {
                MessageBox.Show("Заполните все поля!");
                return;
            }
            
            //Проверить совпадают ли новые значения в полях, со старыми!
            if (
               Name_txbx.Text.Trim() == oldBook.Name &&
               Publicher_txbx.Text.Trim() == oldBook.Publicher &&
               PublicherDate_txbx.Text.Trim() == oldBook.PublichedDate &&
               ISBN_txbx.Text.Trim() == oldBook.ISBN &&
               AuthorName_txbx.Text.Trim() == oldBook.AuthorName
               )
            {
                MessageBox.Show("Инфа о книге не была изменена!");
                return;
            }

            this.NewBook = new Book();
            NewBook.AuthorName = AuthorName_txbx.Text.Trim();
            NewBook.ID = oldBook.ID;
            NewBook.ISBN = ISBN_txbx.Text.Trim();
            NewBook.Name = Name_txbx.Text.Trim();
            NewBook.Publicher = Publicher_txbx.Text.Trim();
            NewBook.PublichedDate = PublicherDate_txbx.Text.Trim();
            //Если все ОК
            DialogResult = DialogResult.OK;
        }

        private void UpdateBook_Load(object sender, EventArgs e)
        {
            ID_txbx.Text = oldBook.ID.ToString();
            Name_txbx.Text = oldBook.Name;
            Publicher_txbx.Text = oldBook.Publicher;
            PublicherDate_txbx.Text = oldBook.PublichedDate;
            ISBN_txbx.Text = oldBook.ISBN;
            AuthorName_txbx.Text = oldBook.AuthorName;
        }
    }
}
