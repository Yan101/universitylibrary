﻿namespace Librury
{
    partial class NewMagazine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.save_magazine_btn = new System.Windows.Forms.Button();
            this.Add_magazine_btn = new System.Windows.Forms.Button();
            this.Name_txbx = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Publicher_txbx = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.PublicherDate_txbx = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.IssueNum_txbx = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.countMag_lbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // save_magazine_btn
            // 
            this.save_magazine_btn.Location = new System.Drawing.Point(85, 145);
            this.save_magazine_btn.Name = "save_magazine_btn";
            this.save_magazine_btn.Size = new System.Drawing.Size(109, 23);
            this.save_magazine_btn.TabIndex = 0;
            this.save_magazine_btn.Text = "Save Magazine";
            this.save_magazine_btn.UseVisualStyleBackColor = true;
            this.save_magazine_btn.Click += new System.EventHandler(this.save_magazine_btn_Click);
            // 
            // Add_magazine_btn
            // 
            this.Add_magazine_btn.Location = new System.Drawing.Point(85, 116);
            this.Add_magazine_btn.Name = "Add_magazine_btn";
            this.Add_magazine_btn.Size = new System.Drawing.Size(109, 23);
            this.Add_magazine_btn.TabIndex = 1;
            this.Add_magazine_btn.Text = "Add Magazine";
            this.Add_magazine_btn.UseVisualStyleBackColor = true;
            this.Add_magazine_btn.Click += new System.EventHandler(this.Add_magazine_btn_Click);
            // 
            // Name_txbx
            // 
            this.Name_txbx.Location = new System.Drawing.Point(85, 12);
            this.Name_txbx.Name = "Name_txbx";
            this.Name_txbx.Size = new System.Drawing.Size(109, 20);
            this.Name_txbx.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(52, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Title";
            // 
            // Publicher_txbx
            // 
            this.Publicher_txbx.Location = new System.Drawing.Point(85, 38);
            this.Publicher_txbx.Name = "Publicher_txbx";
            this.Publicher_txbx.Size = new System.Drawing.Size(109, 20);
            this.Publicher_txbx.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Publicher";
            // 
            // PublicherDate_txbx
            // 
            this.PublicherDate_txbx.Location = new System.Drawing.Point(85, 64);
            this.PublicherDate_txbx.Name = "PublicherDate_txbx";
            this.PublicherDate_txbx.Size = new System.Drawing.Size(109, 20);
            this.PublicherDate_txbx.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Publicher Date";
            // 
            // IssueNum_txbx
            // 
            this.IssueNum_txbx.Location = new System.Drawing.Point(85, 90);
            this.IssueNum_txbx.Name = "IssueNum_txbx";
            this.IssueNum_txbx.Size = new System.Drawing.Size(109, 20);
            this.IssueNum_txbx.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Issue Number";
            // 
            // countMag_lbl
            // 
            this.countMag_lbl.AutoSize = true;
            this.countMag_lbl.Location = new System.Drawing.Point(37, 145);
            this.countMag_lbl.Name = "countMag_lbl";
            this.countMag_lbl.Size = new System.Drawing.Size(19, 13);
            this.countMag_lbl.TabIndex = 4;
            this.countMag_lbl.Text = "__";
            // 
            // NewMagazine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(203, 177);
            this.Controls.Add(this.countMag_lbl);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.IssueNum_txbx);
            this.Controls.Add(this.PublicherDate_txbx);
            this.Controls.Add(this.Publicher_txbx);
            this.Controls.Add(this.Name_txbx);
            this.Controls.Add(this.Add_magazine_btn);
            this.Controls.Add(this.save_magazine_btn);
            this.Name = "NewMagazine";
            this.Text = "NewMagazine";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button save_magazine_btn;
        private System.Windows.Forms.Button Add_magazine_btn;
        private System.Windows.Forms.TextBox Name_txbx;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Publicher_txbx;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox PublicherDate_txbx;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox IssueNum_txbx;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label countMag_lbl;
    }
}