﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Common;

namespace Librury
{
    public partial class LogIn : Form
    {
        public string ConnectionParameters { get; private set; }
        public SourceType DBType { get; private set; }

        public LogIn()
        {
            InitializeComponent();
            ConnectionString_txbx.Text = "Data Source=;Initial Catalog=;uid=;pwd=";
        }

        private void SQL_rdbtn_CheckedChanged(object sender, EventArgs e)
        {
            XML_gpbx.Enabled = false;
            SQL_gpbx.Enabled = true;
        }

        private void XML__rdbtn_CheckedChanged(object sender, EventArgs e)
        {
                XML_gpbx.Enabled = true;
                SQL_gpbx.Enabled = false;
        }

        private void Cancel_btn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ConnectionParam_txbx_Leave(object sender, EventArgs e)
        {
            if (UseWinAuth_chbx.Checked)
            {
                Login_txbx.Enabled = Password_txbx.Enabled = false;
                ConnectionString_txbx.Text = String.Format(
                   "Data Source={0};Initial Catalog={1};Integrated Security=True",
                   DataSource_txbx.Text.Trim(), InitialCatalog_txbx.Text.Trim());
            }
            else
            {
                Login_txbx.Enabled = Password_txbx.Enabled = true;
                //Логин/Пароль!
                ConnectionString_txbx.Text = String.Format(
                    "Data Source={0};Initial Catalog={1};uid={2};pwd={3}",
                     DataSource_txbx.Text.Trim(), InitialCatalog_txbx.Text.Trim(), Login_txbx.Text.Trim(), Password_txbx.Text.Trim());
            }
        }

        private void ChooseXmlFile_btn_Click(object sender, EventArgs e)
        {
            OpenFileDialog openXML_fldg = new OpenFileDialog();
            openXML_fldg.FileName = string.Empty;
            openXML_fldg.Filter = "File XML (*.xml)|*.xml|All Files (*.*)|*.*";
            if (openXML_fldg.ShowDialog() == DialogResult.OK)
            {
                XmlFile_txbx.Text = openXML_fldg.FileName;   
            }
        }

        private void Connect_btn_Click(object sender, EventArgs e)
        {
            if (XML__rdbtn.Checked)
            {
                this.ConnectionParameters = this.XmlFile_txbx.Text.Trim();
                this.DBType = SourceType.XML;
            }
            else
            {
                if (DataSource_txbx.Text.Trim().Length == 0 || InitialCatalog_txbx.Text.Trim().Length == 0)
                {
                    MessageBox.Show("You do not set Location server And Target DB");
                    return;
                }

                if (!UseWinAuth_chbx.Checked)
                    if (Login_txbx.Text.Trim().Length == 0 || Password_txbx.Text.Trim().Length == 0)
                    {
                        MessageBox.Show("You do not set Login/Password");
                        return;
                    }
                ConnectionParameters = ConnectionString_txbx.Text.Trim();
                DBType = SourceType.SQL;
            }
            DialogResult = DialogResult.OK;
        }

        private void LogIn_Load(object sender, EventArgs e)
        {
            UseWinAuth_chbx.Checked = true;
            SQL_rdbtn.Checked = true;
            SQL_rdbtn_CheckedChanged(null, null);
            //Connect_btn_Click(null,null);
        }

        private void DataSource_txbx_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
