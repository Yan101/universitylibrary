﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Common;

namespace Librury
{
    //#1 Пользовательский делегат
    public delegate void SelectItem(object sender, EventArgs e);

    //Работа событий. при общении одной формы с другой(обратный процесс - уведомлениями)
    //При загрузке формы, получаем список журналов, а при выборе названия журнала, получим все его данные.
    public partial class NewArticle : Form
    {
        public ListBox IssueNumLtbx { get { return MagazIssueNumer_ltbx; } } //открытое св-во, т.к. будем заполнять через событие выбора журналов в выпадающем списке!
        public List<Article> NewArticles { get; private set; }
 
        //#2 пользоват событие
        public event SelectItem SelectMagName;
        public NewArticle(List<string> magazinesNames) //только названия Журналов
        {
            InitializeComponent();
            MagazName_ltbx.DataSource = magazinesNames;
            NewArticles = new List<Article>();
        }

        private void SaveArticle_btn_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
        //GetAllMAgazineName ( получать название всех журналов) GetCurrentMagazinAllIssueNumber (получать все выпуски журналов по конкретному имени)
        private void Add_Article_btn_Click(object sender, EventArgs e)
        {
            string name = NameArt_txbx.Text.Trim();
            string publicher = PublicherArt_txbx.Text.Trim();
            string publicherDate = PublicherDateArt_txbx.Text.Trim();
            string version = VersionArt_txbx.Text.Trim();
            string authorName = AuthorNameArt_txbx.Text.Trim();

            string magName = MagazName_ltbx.SelectedValue.ToString();
            string magIssueNum = "";
            if (MagazIssueNumer_ltbx.Items != null && MagazIssueNumer_ltbx.SelectedItem != null)
                magIssueNum = MagazIssueNumer_ltbx.SelectedValue.ToString();

            //Если хотя бы кто-то Empty, то..
            if (name == string.Empty || publicher == string.Empty || publicherDate == string.Empty ||
                version == string.Empty || authorName == string.Empty || magName == string.Empty ||
                magIssueNum == string.Empty)
            {
                MessageBox.Show("Не все поля указанны");
                return;
            }

            foreach (Article art in NewArticles)
            {
                //Проверить уникальность. Если совпадает название статьи, версия + название и номер журнала, тогда нельзя добавить.
                if (name == art.Name && magName == art.MagazineName && magIssueNum == art.MagazineIssueNumber )
                {
                    MessageBox.Show("такая стать уже была добавлена!");
                    return;
                }
            }

            //Иначе добавить новую статью!
            NewArticles.Add(new Article(Guid.NewGuid(),name, publicher, publicherDate, magName, magIssueNum, authorName, version));
            addedArticles_lbl.Text = "Добавленных статей: " + NewArticles.Count;
            return;
        }

        private void MagazName_ltbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SelectMagName != null) //Т.е. если на событие кто-нибудь подписан
                SelectMagName(sender, e); //тогда генерируем это событие! Используя sender, e - мы сможем понять для какого именно журнала нужно извлечь инфу!!!
        }

        private void NewArticle_Load(object sender, EventArgs e)
        {

        }

    }
}