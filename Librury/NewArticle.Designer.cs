﻿namespace Librury
{
    partial class NewArticle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SaveArticle_btn = new System.Windows.Forms.Button();
            this.AuthorNameArt_txbx = new System.Windows.Forms.TextBox();
            this.AuthorName = new System.Windows.Forms.Label();
            this.VersionArt_txbx = new System.Windows.Forms.TextBox();
            this.ISBN = new System.Windows.Forms.Label();
            this.PublicherDateArt_txbx = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.PublicherArt_txbx = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.NameArt_txbx = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.addedArticles_lbl = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Add_Article_btn = new System.Windows.Forms.Button();
            this.MagazName_ltbx = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.MagazIssueNumer_ltbx = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // SaveArticle_btn
            // 
            this.SaveArticle_btn.Location = new System.Drawing.Point(134, 311);
            this.SaveArticle_btn.Name = "SaveArticle_btn";
            this.SaveArticle_btn.Size = new System.Drawing.Size(165, 23);
            this.SaveArticle_btn.TabIndex = 21;
            this.SaveArticle_btn.Text = "Save Article";
            this.SaveArticle_btn.UseVisualStyleBackColor = true;
            this.SaveArticle_btn.Click += new System.EventHandler(this.SaveArticle_btn_Click);
            // 
            // AuthorNameArt_txbx
            // 
            this.AuthorNameArt_txbx.Location = new System.Drawing.Point(134, 116);
            this.AuthorNameArt_txbx.Name = "AuthorNameArt_txbx";
            this.AuthorNameArt_txbx.Size = new System.Drawing.Size(165, 20);
            this.AuthorNameArt_txbx.TabIndex = 20;
            // 
            // AuthorName
            // 
            this.AuthorName.AutoSize = true;
            this.AuthorName.Location = new System.Drawing.Point(59, 119);
            this.AuthorName.Name = "AuthorName";
            this.AuthorName.Size = new System.Drawing.Size(69, 13);
            this.AuthorName.TabIndex = 19;
            this.AuthorName.Text = "Author Name";
            // 
            // VersionArt_txbx
            // 
            this.VersionArt_txbx.Location = new System.Drawing.Point(134, 90);
            this.VersionArt_txbx.Name = "VersionArt_txbx";
            this.VersionArt_txbx.Size = new System.Drawing.Size(165, 20);
            this.VersionArt_txbx.TabIndex = 18;
            // 
            // ISBN
            // 
            this.ISBN.AutoSize = true;
            this.ISBN.Location = new System.Drawing.Point(86, 93);
            this.ISBN.Name = "ISBN";
            this.ISBN.Size = new System.Drawing.Size(42, 13);
            this.ISBN.TabIndex = 17;
            this.ISBN.Text = "Version";
            // 
            // PublicherDateArt_txbx
            // 
            this.PublicherDateArt_txbx.Location = new System.Drawing.Point(134, 64);
            this.PublicherDateArt_txbx.Name = "PublicherDateArt_txbx";
            this.PublicherDateArt_txbx.Size = new System.Drawing.Size(165, 20);
            this.PublicherDateArt_txbx.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Publicher Date";
            // 
            // PublicherArt_txbx
            // 
            this.PublicherArt_txbx.Location = new System.Drawing.Point(134, 38);
            this.PublicherArt_txbx.Name = "PublicherArt_txbx";
            this.PublicherArt_txbx.Size = new System.Drawing.Size(165, 20);
            this.PublicherArt_txbx.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(77, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Publicher";
            // 
            // NameArt_txbx
            // 
            this.NameArt_txbx.Location = new System.Drawing.Point(134, 12);
            this.NameArt_txbx.Name = "NameArt_txbx";
            this.NameArt_txbx.Size = new System.Drawing.Size(165, 20);
            this.NameArt_txbx.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(45, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Magazine Name";
            // 
            // addedArticles_lbl
            // 
            this.addedArticles_lbl.AutoSize = true;
            this.addedArticles_lbl.Location = new System.Drawing.Point(12, 292);
            this.addedArticles_lbl.Name = "addedArticles_lbl";
            this.addedArticles_lbl.Size = new System.Drawing.Size(83, 13);
            this.addedArticles_lbl.TabIndex = 19;
            this.addedArticles_lbl.Text = "AddedArticle_lbl";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 205);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Magazine Issue Num";
            // 
            // Add_Article_btn
            // 
            this.Add_Article_btn.Location = new System.Drawing.Point(134, 282);
            this.Add_Article_btn.Name = "Add_Article_btn";
            this.Add_Article_btn.Size = new System.Drawing.Size(165, 23);
            this.Add_Article_btn.TabIndex = 21;
            this.Add_Article_btn.Text = "Add Article";
            this.Add_Article_btn.UseVisualStyleBackColor = true;
            this.Add_Article_btn.Click += new System.EventHandler(this.Add_Article_btn_Click);
            // 
            // MagazName_ltbx
            // 
            this.MagazName_ltbx.FormattingEnabled = true;
            this.MagazName_ltbx.Location = new System.Drawing.Point(135, 143);
            this.MagazName_ltbx.Name = "MagazName_ltbx";
            this.MagazName_ltbx.Size = new System.Drawing.Size(164, 56);
            this.MagazName_ltbx.TabIndex = 22;
            this.MagazName_ltbx.SelectedIndexChanged += new System.EventHandler(this.MagazName_ltbx_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(101, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Title";
            // 
            // MagazIssueNumer_ltbx
            // 
            this.MagazIssueNumer_ltbx.FormattingEnabled = true;
            this.MagazIssueNumer_ltbx.Location = new System.Drawing.Point(134, 205);
            this.MagazIssueNumer_ltbx.Name = "MagazIssueNumer_ltbx";
            this.MagazIssueNumer_ltbx.Size = new System.Drawing.Size(165, 56);
            this.MagazIssueNumer_ltbx.TabIndex = 22;
            // 
            // NewArticle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(316, 427);
            this.Controls.Add(this.MagazIssueNumer_ltbx);
            this.Controls.Add(this.MagazName_ltbx);
            this.Controls.Add(this.Add_Article_btn);
            this.Controls.Add(this.SaveArticle_btn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.addedArticles_lbl);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.AuthorNameArt_txbx);
            this.Controls.Add(this.AuthorName);
            this.Controls.Add(this.VersionArt_txbx);
            this.Controls.Add(this.ISBN);
            this.Controls.Add(this.PublicherDateArt_txbx);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.PublicherArt_txbx);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.NameArt_txbx);
            this.Name = "NewArticle";
            this.Text = "NewArticle";
            this.Load += new System.EventHandler(this.NewArticle_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SaveArticle_btn;
        private System.Windows.Forms.TextBox AuthorNameArt_txbx;
        private System.Windows.Forms.Label AuthorName;
        private System.Windows.Forms.TextBox VersionArt_txbx;
        private System.Windows.Forms.Label ISBN;
        private System.Windows.Forms.TextBox PublicherDateArt_txbx;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox PublicherArt_txbx;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox NameArt_txbx;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label addedArticles_lbl;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button Add_Article_btn;
        private System.Windows.Forms.ListBox MagazName_ltbx;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox MagazIssueNumer_ltbx;
    }
}