﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Common;

namespace Librury
{
    public partial class UsersBorrowed : Form
    {
        public event SelectItem SelectUserName;
        public DataGridView AllBorrowedCopieByUser_dgv { get { return allBorrowedCopieByUser_dgv; }} //чтобы главная форма могла установить DataSource для этого DGV
        public List<Guid> retunedItem;
        public UsersBorrowed(List<User> users)
        {
            InitializeComponent();
            
            allUsers_ltbx.DataSource = users;
            allUsers_ltbx.DisplayMember = "Name";
            allUsers_ltbx.ValueMember = "ID";
            retunedItem = new List<Guid>();
        }

        private void allUsers_ltbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            //чтобы сохранить принцип инкапсуляции и не давать доступ к DataSet и к некоторым "Листам" иэ класса UsersBorrowed, исп-ем Событие. 
            //На него подписывается Главная форма, и из нее уже идет заполнение DGV -> DGV должен быть public или иметь public-св-во(только для чтения).
            if (SelectUserName != null)
                SelectUserName(sender, e); //sender - LISTBOX;
        }

        private void allBorrowedCopieByUser_dgv_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            copieID_txbx.Text = allBorrowedCopieByUser_dgv["CopieID", e.RowIndex].Value.ToString(); //e.RowIndex - активная строка!
        }

        private void ReturnItem_btn_Click(object sender, EventArgs e)
        {
            if (copieID_txbx.Text!=string.Empty)
                retunedItem.Add(new Guid(copieID_txbx.Text));
        }

        private void close_btn_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
