﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public enum LibruryAction
    {
        AddBook,
        UpdateBook,
        DeleteBook,

        AddArticle,
        UpdateArticle,
        DeleteArticle,

        AddMagazine,
        UpdateMagazine,
        DeleteMagazine,

        AddUser,
        UpdateUser,
        DeleteUser
    }
}
