﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Copie
    {
        public Guid ID { get; private set; }
        public Guid ItemID { get; private set; }
        public Boolean IsBorrowed { get; private set; }

        public Copie(Guid id, Guid itemID, bool isBorrowed)
        {
            ID = id;
            ItemID = itemID;
            IsBorrowed = isBorrowed;
        }
    }
}