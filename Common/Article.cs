﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Article : Item
    {
        public Guid ID { get; set; }
        public string MagazineName { get; set; }
        public string MagazineIssueNumber { get; set; }
        public string AuthorName { get; set; }
        public string Version { get; set; }
        
        public Dictionary<string, string> ItemFields { get; private set; }
        public Dictionary<string, string> MagazineFields { get; private set; }
        public Dictionary<string, string> AuthorFields { get; private set; }
        public Dictionary<string, string> ArticleFields { get; private set; }

        public Article()    {   }
        public Article(string name, string Publicher, string PublichedDate, string magazineName, string magazineIssueNumber, string authorName, string version) 
            : this (Guid.Empty, name, Publicher, PublichedDate, magazineName, magazineName, authorName, version) { }

        public Article(Guid id, string name, string Publicher, string PublichedDate,
            string magazineName, string magazineIssueNumber, string authorName,
            string version) 
        {
            this.ID = id;
            this.Name = name;
            this.Publicher = Publicher;
            this.PublichedDate = PublichedDate;
            this.MagazineName = magazineName;
            this.MagazineIssueNumber = magazineIssueNumber;
            this.AuthorName = authorName;
            this.Version = version;

            ItemFields = new Dictionary<string, string>(3);
            ItemFields.Add("Name", Name);
            ItemFields.Add("Publicher", Publicher);
            ItemFields.Add("PublichedDate", PublichedDate.ToString());

            AuthorFields = new Dictionary<string, string>(1);
            AuthorFields.Add("Name", AuthorName);

            MagazineFields = new Dictionary<string, string>(2);
            MagazineFields.Add("Name", MagazineName);
            MagazineFields.Add("IssueNumber", MagazineIssueNumber);

            ArticleFields = new Dictionary<string, string>(1);
            ArticleFields.Add("Version", Version);
        }
    }
}
