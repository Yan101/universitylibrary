﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    //Этот класс ничего не должен знать о структуре БД
    public class Book : Item  
    {
        public Guid  ID { get; set; }
        public string ISBN { get; set; }
        public string AuthorName { get; set; } //Имя специально такое

        //Эти объекты предсталяют собой разъединенные данные
        //Типизированные коллекции Dictionary
        //Когда биндим коллекцию к DGV, то туда грузятся все открытые члены!! 
        public Dictionary<string, string> ItemFields { get; private set; }  //Поля будут касатся таблицы ItemFields
        public Dictionary<string, string> AuthorFields { get; private set; }
        public Dictionary<string, string> BookFields { get; private set; }

        public Book()   {   }

        public Book(string name, string Publicher, string PublichedDate, string isbn, string authorName) 
            : this (Guid.Empty, name, Publicher, PublichedDate, isbn, authorName) {   }

        public Book(Guid id, string name, string Publicher, string PublichedDate, string isbn, string authorName)
        {
            this.ID = id;
            this.Name = name;
            this.Publicher = Publicher;
            this.PublichedDate = PublichedDate;
            this.ISBN = isbn;
            this.AuthorName = authorName;
            
            //Теже значения, только перегруппированы в словари!
            ItemFields = new Dictionary<string, string>(3);
            ItemFields.Add("Name", Name);
            ItemFields.Add("Publicher", Publicher);
            ItemFields.Add("PublichedDate", PublichedDate.ToString());
            
            AuthorFields = new Dictionary<string, string>(1);
            AuthorFields.Add("Name", AuthorName);

            BookFields = new Dictionary<string, string>(1);
            BookFields.Add("ISBN", ISBN);
        }
    }
}
