﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Magazine : Item
    {
        public Guid ID { get; set; }
        public string IssueNumber { get; set; }

        public Dictionary<string, string> ItemFields { get; private set; }
        public Dictionary<string, string> MagazineFields { get; private set; }

        public Magazine()   {   }

        public Magazine(string name, string Publicher, string PublicherDate,
            string  issueNumber)
            : this(Guid.Empty, name, Publicher, PublicherDate, issueNumber) {    }

        public Magazine(Guid id, string name, string Publicher, string PublichedDate,
            string issueNumber)
        {
            this.ID = id;
            this.Name = name;
            this.Publicher = Publicher;
            this.PublichedDate = PublichedDate;
            this.IssueNumber = issueNumber;

            ItemFields = new Dictionary<string, string>(3);
            ItemFields.Add("Name", Name);
            ItemFields.Add("Publicher", Publicher);
            ItemFields.Add("PublichedDate", PublichedDate.ToString());

            MagazineFields = new Dictionary<string, string>(2);
            MagazineFields.Add("IssueNumber", IssueNumber);
        }
    }
}
