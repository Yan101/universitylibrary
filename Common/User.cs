﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Security.Cryptography;

namespace Common
{
    public class User
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public Guid Password { get; set; }
        public bool IsAdmin { get; set; }

        public User(Guid id, string name, string userName, Guid password, bool isAdmin)
        {
            this.ID = id;
            this.Name = name;
            this.UserName = userName;
            this.Password = password;
            this.IsAdmin = isAdmin;
        }
        public static Guid GetHashString(string s)
        {
            //получили массив байтов
            byte[] byteValue = Encoding.Unicode.GetBytes(s); //байты представляют стринговые значения

            //теперь шифруем через MD5
            MD5CryptoServiceProvider csp = new MD5CryptoServiceProvider();

            byte[] byteHash = csp.ComputeHash(byteValue); //байты представляют зашифрованные значения

            string result = string.Empty;

            foreach (byte b in byteHash)
                result += string.Format("{0:x2}", b);

            return new Guid(result);
        }
    }
}
