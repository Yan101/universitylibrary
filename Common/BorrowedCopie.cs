﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    //Этот класс предоставляет конкретную копию, которая находится на руках.
    public class BorrowedCopie
    {
        public Guid CopieID { get; private set; }
        public Guid UserID { get; private set; }
        public DateTime BorrowedDate { get; private set; }
        public string BorrowedItemName { get; private set; }

        public BorrowedCopie(Guid copieID, Guid userID, DateTime borrowedDate, string borrowedItemName)
        {
            CopieID = copieID;
            UserID = userID;
            BorrowedDate = borrowedDate;
            BorrowedItemName = BorrowedItemName;
        }
    }
}
