﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public abstract class Item
    {
        public string Name { get; set; }
        public string Publicher { get; set; }
        public string PublichedDate { get; set; }
    }
}
